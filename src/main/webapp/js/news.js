(function () {
    $(document).ready(function () {
        var hasNextPage = $("#has-next-page").val();
        var page = $(".nav span").text();
        if (page === "1") {
            $("#previous").addClass("disabled-link");
        }
        if (hasNextPage === "false") {
            $("#next").addClass("disabled-link");
        }

        $("#show-form").click(function () {
            var newNews = $("#new-news");
            if ($(newNews).css("display") == "none") {
                $(newNews).css("display", "flex");
            } else {
                $(newNews).hide();
            }
        });
    });
})();