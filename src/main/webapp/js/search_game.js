$(document).ready(function () {
    var Application = {
        URL: "http://localhost:8080/game",
        PAGE_GAME: "/jsp/game/game.jsp",
        COMMAND_SEARCH: "search_opponent",
        COMMAND_WAIT: "wait_player",
        COMMAND_CANCEL: "cancel_search",
        isCanceled: false
    };

    $("#search-opponent").on("click", search);
    $("#cancel-search").on("click", cancel);

    function search() {
        $("#progress-search").show();
        $("#start-search").hide();
        $.ajax({
            url: Application.URL,
            type: "POST",
            data: {
                "command": Application.COMMAND_SEARCH
            }
        }).done(
            /**
             * @param {Object} data - Response from server
             */
            function (data) {
                if (data === "null") {
                    setTimeout(continueSearch, 1000);
                } else {
                    window.location = Application.PAGE_GAME;
                }
            }
        );
    }

    function continueSearch() {
        if (!Application.isCanceled) {
            $.ajax({
                url: Application.URL,
                type: "GET",
                data: {
                    "command": Application.COMMAND_WAIT
                }
            }).done(
                function (data) {
                    if (data === "null") {
                        setTimeout(continueSearch, 1000);
                    } else {
                        window.location = Application.PAGE_GAME;
                    }
                }
            );
        }
    }

    function cancel() {
        Application.isCanceled = true;
        $("#progress-search").hide();
        $("#start-search").show();
        $.ajax({
            url: Application.URL,
            type: "GET",
            data: {
                "command": Application.COMMAND_CANCEL
            }
        });
    }

    function disableF5(e) {
        if ((e.which || e.keyCode) == 116) {
            e.preventDefault();
        }
    }

    $(document).on("keydown", disableF5);

    $(window).on("beforeunload", function () {
        cancel();
    });
});
