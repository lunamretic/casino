(function() {
    $(document).ready(function () {
        $("#cancel-email").click(function() {cancelChange("email")});
        $("#change-email").click(function() {change("email")});
        $("#cancel-name").click(function() {cancelChange("name")});
        $("#change-name").click(function() {change("name")});
        $("#cancel-password").click(function() {cancelChange("password")});
        $("#change-password").click(function() {change("password")});

        function change(parameter) {
            $("#input-" + parameter.toString()).prop("disabled", false);
            $(".change-" + parameter.toString()).show();
            $("#change-" + parameter.toString()).hide();
        }

        function cancelChange(parameter) {
            $("#input-" + parameter.toString()).prop("disabled", true ).val("");
            $(".change-" + parameter.toString()).hide();
            $("#change-" + parameter.toString()).show();
        }

        $("#input-repeat-new-password").change(function () {
            var invalidInput = $("#invalid-password-repeat");
            invalidInput.text(this.title);
            if (this.value == $("#input-new-password").val()) {
                invalidInput.hide();
                this.setCustomValidity("");
            } else {
                invalidInput.show();
                this.setCustomValidity(this.title);
            }
        });
    });
})();