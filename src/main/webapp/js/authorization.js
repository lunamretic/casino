(function () {
    $(document).ready(function () {
        $("#username").change(function () {
            validateInput(this);
        });

        $("#email").change(function () {
            validateInput(this);
        });

        $("#password").change(function () {
            validateInput(this);
        });

        $("#password-repeat").change(function () {
            var invalidInput = $('#invalid-password-repeat');
            invalidInput.text(this.title);
            if (this.value == $('#password').val()) {
                invalidInput.hide();
                this.setCustomValidity("");
            } else {
                invalidInput.show();
                this.setCustomValidity(this.title);
            }
        });

        function validateInput(form) {
            var invalidInput = $('#invalid-' + form.name);
            invalidInput.text(form.title);
            if (form.checkValidity()) {
                invalidInput.hide();
            } else {
                invalidInput.show();
            }
        }
    });
})();