$(document).ready(function () {
    var Application = {
        URL: "http://localhost:8080/game",
        MIN_REQUIRED_CASH: 30,
        DEFINE_TURN_COMMAND: "game_define_turn",
        POLLING_COMMAND: "game_polling",
        PLACE_BET_COMMAND: "game_place_bet",
        ACCEPT_BET_COMMAND: "game_accept_bet",
        REFUSE_BET_COMMAND: "game_refuse_bet",
        HAND_COMMAND: "game_start_hand",
        DRAW_CARD_COMMAND: "game_draw_card",
        STAND_COMMAND: "game_stand",
        STAND_AI_COMMAND: "game_stand_ai",
        NEW_GAME_COMMAND: "game_offer_new_game",
        LEAVE_GAME_COMMAND: "game_leave",
        isPVP: false,
        bet: 0
    };

    turn();

    function turn() {
        $.ajax({
            url: Application.URL,
            type: "POST",
            data: {
                "command": Application.DEFINE_TURN_COMMAND
            }
        }).done(
            /**
             * @param {Object} data - Response from server
             * @param {boolean} data.playerTurn - True, if current turn is player's turn. Otherwise - false.
             * @param {boolean} data.ai - True, if current game vs AI
             */
            function (data) {
                var response = $.parseJSON(data);
                var isPlayerTurn = response.playerTurn;
                var isAI = response.ai;
                if (!isAI) {
                    Application.isPVP = true;
                    if (isPlayerTurn) {
                        $("#place-bet-box").css("display", "flex");
                    } else {
                        polling();
                    }
                } else {
                    $("#place-bet-box").css("display", "flex");
                }
            });
    }

    function polling() {
        $("#wait-bar").show();
        $.ajax({
            url: Application.URL,
            type: "POST",
            data: {
                "command": Application.POLLING_COMMAND
            }
        }).done(
            function (data) {
                if (data === "null") {
                    setTimeout(polling, 1000);
                } else {
                    $("#wait-bar").hide();
                    var response = $.parseJSON(data);
                    var type = response.type;
                    switch (type) {
                        case "offered-bet":
                            Application.bet = response.bet;
                            $("#bet-box").show();
                            $("#suggested-bet").text(Application.bet);
                            break;
                        case "bet-accepted":
                        case "stand":
                            getStartHand();
                            break;
                        case "bet-refuse":
                            $("#result-draw").show();
                            $("#bet-refused").show();
                            break;
                        case "hand":
                            $("#opponent-cards").append('' +
                                '<li><img src="/img/game/deck.png" class="opponent-card"></li>' +
                                '<li><img src="/img/game/deck.png" class="opponent-card"></li>');
                            polling();
                            break;
                        case "card":
                            $("#opponent-cards").append('' +
                                '<li><img src="/img/game/deck.png" class="opponent-card"></li>');
                            var $opponentCards = $(".opponent-card");
                            if ($opponentCards.length > 3) {
                                $opponentCards.addClass("four");
                                $opponentCards.first().removeClass("four");
                            }
                            polling();
                            break;
                        case "results":
                            showResults(data);
                            break;
                        case "new-game":
                            prepareNewGame();
                            turn();
                            break;
                        case "user-left":
                            $("#result-win").show();
                            $("#user-left").show();
                            break;
                        case "game-finished":
                            $("#user-left").show();
                            break;
                        default:
                    }
                }
            }
        );
    }

    $("#place-bet").click(function () {
        var bet = $("#bet").val();
        $("#place-bet-box").hide();

        $.ajax({
            url: Application.URL,
            type: "GET",
            data: {
                "command": Application.PLACE_BET_COMMAND,
                "bet": bet
            }
        }).done(
            function () {
                if (Application.isPVP) {
                    polling();
                } else {
                    getStartHand();
                }
            }
        );
    });

    $("#accept-bet").click(function () {
        $("#bet-box").hide();
        $.ajax({
            url: Application.URL,
            type: "POST",
            data: {
                "command": Application.ACCEPT_BET_COMMAND
            }
        }).done(function () {
            polling();
        });
    });

    $("#refuse-bet").click(function () {
        $("#bet-box").hide();
        $.ajax({
            url: Application.URL,
            type: "POST",
            data: {
                "command": Application.REFUSE_BET_COMMAND
            }
        }).done(
            function () {
                $("#result-draw").show();
                $("#bet-refused").show();
            }
        );

    });

    function getStartHand() {
        $.ajax({
            url: Application.URL,
            type: "GET",
            data: {"command": Application.HAND_COMMAND}
        }).done(
            /**
             * @param {Object} data - Response from server
             * @param {Object[]} data.startHand - Player cards
             * @param {int} data.points - User points
             */
            function (data) {
                var response = $.parseJSON(data);
                var hand = response.startHand;
                var points = response.points;
                $("#user-points").text(points);
                /**
                 * @param {Object} card - Game card
                 * @param {string} card.imgSrc - Src to card image
                 * @param {Object} card.suit - Suit of the card
                 * @param {Object} card.rank - Rank of the card
                 */
                $.each(hand, function (index, card) {
                    $("#user-cards").append('' +
                        '<li><img src="' + card.imgSrc + '" alt="' + card.suit + ' ' + card.rank + '" class="card"></li>');
                });
                $("#control-box").css("display", "flex");
                if (points >= 21) {
                    $("#draw-card").prop("disabled", true);
                }
            }
        );
    }

    $("#draw-card").click(function () {
        $.ajax({
            url: Application.URL,
            type: "POST",
            data: {"command": Application.DRAW_CARD_COMMAND}
        }).done(
            /**
             * @param {Object} data - Response from server
             * @param {Object} data.card - Received card
             * @param {int} data.points - User points
             */
            function (data) {
                var response = $.parseJSON(data);
                var card = response.card;
                var points = response.points;
                $("#user-points").text(points);
                /**
                 * @param {Object} card - Game card
                 * @param {string} card.imgSrc - Src to card image
                 * @param {Object} card.suit - Suit of the card
                 * @param {Object} card.rank - Rank of the card
                 */
                var $userCards = $("#user-cards");
                $userCards.append('' +
                    '<li><img src="' + card.imgSrc + '" alt="' + card.suit + ' ' + card.rank + '" class="card"></li>');
                var $cards = $(".card");
                if ($cards.length === 4) {
                    $cards.addClass("four");
                    $cards.first().removeClass("four");
                }
                if ($cards.length === 5) {
                    $cards.addClass("five");
                    $cards.first().removeClass("five");
                }
                if (points >= 21 || $cards.length === 5) {
                    $("#draw-card").prop("disabled", true);
                }
            }
        )
    });

    $("#stand").click(function () {
        $("#draw-card").prop("disabled", true);
        $("#stand").prop("disabled", true);

        var command;
        if (Application.isPVP) {
            command = Application.STAND_COMMAND;
        } else {
            command = Application.STAND_AI_COMMAND;
        }

        $.ajax({
            url: Application.URL,
            type: "GET",
            data: {
                "command": command
            }
        }).done(
            function (data) {
                if (!Application.isPVP) {
                    showResults(data);
                } else {
                    if (data === "null") {
                        polling();
                    } else {
                        showResults(data);
                    }
                }
            }
        )
    });

    /**
     * @param {Object} data - Response from server
     * @param {Object[]} data.opponentHand - Opponent cards
     * @param {int} data.opponentPoints - Opponent points
     * @param {int} data.cash - Your updated cash
     * @param {string} data.message - Game results
     */
    function showResults(data) {
        var response = $.parseJSON(data);
        var opponentHand = response.opponentHand;
        var message = response.message;
        var points = response.opponentPoints;
        var updatedCash = response.cash;
        var $cash = $("#cash");
        $("#opponent-points").text(points);
        /**
         * @param {Object} card - Game card
         * @param {string} card.imgSrc - Src to card image
         * @param {Object} card.suit - Suit of the card
         * @param {Object} card.rank - Rank of the card
         */
        $("#opponent-cards").empty();
        $.each(opponentHand, function (index, card) {
            $("#opponent-cards").append('' +
                '<li><img src="' + card.imgSrc + '" alt="' + card.suit + ' ' + card.rank + '" class="opponent-card"></li>');
        });
        var $opponentCards = $(".opponent-card");
        if ($opponentCards.length > 3) {
            $opponentCards.addClass("four");
            $opponentCards.first().removeClass("four");
        }
        switch (message) {
            case "draw":
                $("#result-draw").show();
                break;
            case "lose":
                $("#result-lose").show();
                break;
            case "win":
                $("#result-win").show();
                break;
            default:
        }
        $("#new-game-box").css("display", "flex");
        $cash.text(updatedCash);
        if ($cash.text() < Application.MIN_REQUIRED_CASH) {
            $("#new-game").disable();
        }
    }

    $("#new-game").click(function () {
        $("#new-game-box").hide();
        $.ajax({
            url: Application.URL,
            type: "GET",
            data: {"command": Application.NEW_GAME_COMMAND}
        }).done(
            function () {
                if (Application.isPVP) {
                    polling();
                } else {
                    prepareNewGame();
                    $("#place-bet-box").css("display", "flex");
                }
            }
        );
    });

    $("#end-game").click(function () {
        $("#new-game-box").hide();
        $("#game-ended").show();
        leaveGame();
    });

    function leaveGame() {
        $.ajax({
            url: Application.URL,
            type: "GET",
            data: {"command": Application.LEAVE_GAME_COMMAND}
        });
    }

    function prepareNewGame() {
        $("#user-cards").empty();
        $("#opponent-cards").empty();
        $("#user-points").text(0);
        $("#opponent-points").text(0);
        $("#new-game-box").hide();
        $("#control-box").hide();
        $("#result-draw").hide();
        $("#result-lose").hide();
        $("#result-win").hide();
        $("#draw-card").prop("disabled", false);
        $("#stand").prop("disabled", false);
    }

    $(".button-bet").click(function () {
        var $button = $(this);
        var $input = $("#bet");
        var min = 5;
        var max = 30;
        var delta = 5;
        var oldValue = $input.val();
        var newValue;
        if ($button.val() == "+" && oldValue < max) {
            newValue = parseInt(oldValue) + delta;
            $("#bet-minus").prop("disabled", false);
            if (newValue === max) {
                $("#bet-plus").prop("disabled", true);
            }
        } else {
            if ($button.val() == "-" && oldValue > min) {
                newValue = parseInt(oldValue) - delta;
                $("#bet-plus").prop("disabled", false);
                if (newValue === min) {
                    $("#bet-minus").prop("disabled", true);
                }
            } else {
                newValue = min;
                $("#bet-minus").prop("disabled", true);
                $("#bet-plus").prop("disabled", false);
            }
        }
        $input.val(newValue);
    });

    function disableF5(e) {
        if ((e.which || e.keyCode) == 116) {
            e.preventDefault();
        }
    }

    $(document).on("keydown", disableF5);

    $(window).on("beforeunload", function () {
        leaveGame();
    });
});
