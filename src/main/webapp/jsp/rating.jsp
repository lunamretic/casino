<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${not empty sessionScope['locale'] ? sessionScope['locale'] : 'en_UK'}" scope="session" />
<fmt:setBundle basename="pagecontent" />
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><fmt:message key="title.rating"/></title>
    <link href="<c:url value="/css/header.css" />" rel="stylesheet">
    <link href="<c:url value="/css/main.css" />" rel="stylesheet">
    <link href="<c:url value="/css/rating.css" />" rel="stylesheet">
    <link href="<c:url value="/css/footer.css" />" rel="stylesheet">
</head>
<body>
<c:choose>
    <c:when test="${sessionScope.user != null}">
        <%@ include file="/jsp/additional/header.jspf" %>
    </c:when>
    <c:otherwise>
        <%@ include file="/jsp/additional/guest_header.jspf" %>
    </c:otherwise>
</c:choose>
<main>
    <h1><fmt:message key="label.rating"/></h1>
    <table class="top-table">
        <caption><fmt:message key="label.top-10"/></caption>
        <thead>
            <tr>
                <th><fmt:message key="label.number"/></th>
                <th><fmt:message key="label.username"/></th>
                <th><fmt:message key="label.name"/></th>
                <th><fmt:message key="label.win"/></th>
                <th><fmt:message key="label.lose"/></th>
                <th><fmt:message key="label.draw"/></th>
                <th><fmt:message key="label.winrate"/></th>
            </tr>
        </thead>
        <tbody>
        <c:forEach items="${requestScope.topPlayers}" var="player">
            <tr>
                <td data-label="<fmt:message key="label.number"/>">${player.number}</td>
                <td data-label="<fmt:message key="label.username"/>">
                    <c:choose>
                        <c:when test="${sessionScope.user != null}">
                            <a href="${pageContext.request.contextPath}/account?command=profile&username=${player.username}">
                                ${player.username}
                            </a>
                        </c:when>
                        <c:otherwise>
                            ${player.username}
                        </c:otherwise>
                    </c:choose>
                </td>
                <td data-label="<fmt:message key="label.name"/>">${player.name}</td>
                <td data-label="<fmt:message key="label.win"/>">${player.win}</td>
                <td data-label="<fmt:message key="label.lose"/>">${player.lose}</td>
                <td data-label="<fmt:message key="label.draw"/>">${player.draw}</td>
                <td data-label="<fmt:message key="label.winrate"/>">${player.winRate}</td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
</main>
<%@ include file="/jsp/additional/footer.jspf" %>
</body>
</html>