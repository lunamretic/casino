<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${not empty sessionScope['locale'] ? sessionScope['locale'] : 'en_UK'}" scope="session" />
<fmt:setBundle basename="pagecontent" />
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><fmt:message key="title.profile"/></title>
    <link href="<c:url value="/css/header.css" />" rel="stylesheet">
    <link href="<c:url value="/css/main.css" />" rel="stylesheet">
    <link href="<c:url value="/css/profile.css" />" rel="stylesheet">
    <link href="<c:url value="/css/footer.css" />" rel="stylesheet">
</head>
<body>
<%@ include file="/jsp/additional/header.jspf" %>
<main>
    <div class="status-message">
        <c:if test="${requestScope.invalidForm == true}">
            <h4 class="error-message"><fmt:message key="label.invalid-form-username"/></h4>
        </c:if>
    </div>
    <h1><fmt:message key="label.profile"/></h1>
    <c:if test="${sessionScope.user.role == 'ADMIN' && requestScope.infoCard.username != sessionScope.user.username
    && requestScope.userFound == true && requestScope.infoCard.role != 'ADMIN'}">
        <div class="panel-flex-between">
            <div class="admin-control">
                <c:if test="${requestScope.infoCard.role == 'BANNED'}">
                    <form action="${pageContext.request.contextPath}/account" method="POST" >
                        <input type="hidden" name="command" value="update_role">
                        <input type="hidden" name="role" value="user">
                        <input type="hidden" name="username" value="${requestScope.infoCard.username}">
                        <input type="submit" class="button small" value="<fmt:message key="button.unban"/>" />
                    </form>
                </c:if>
                <c:if test="${requestScope.infoCard.role != 'BANNED'}">
                    <form action="${pageContext.request.contextPath}/account" method="POST" >
                        <input type="hidden" name="command" value="update_role">
                        <input type="hidden" name="role" value="banned">
                        <input type="hidden" name="username" value="${requestScope.infoCard.username}">
                        <input type="submit" class="button small" value="<fmt:message key="button.ban"/>" />
                    </form>
                </c:if>
                <form action="${pageContext.request.contextPath}/account" method="POST" >
                    <input type="hidden" name="command" value="update_role">
                    <input type="hidden" name="role" value="admin">
                    <input type="hidden" name="username" value="${requestScope.infoCard.username}">
                    <input type="submit" class="button small" value="<fmt:message key="button.make-admin"/>" />
                </form>
            </div>
            <form class="search-bar" action="${pageContext.request.contextPath}/account" method="GET">
                <input type="hidden" name="command" value="profile">
                <input type="text" name="username" placeholder="<fmt:message key="label.search-user"/>" required
                       title="<fmt:message key="label.valid-username"/>"
                       pattern="^[a-zA-Zа-яА-Я][a-zA-Zа-яА-Я0-9_]{3,20}$"
                >
            </form>
        </div>
    </c:if>
    <c:if test="${sessionScope.user.role != 'ADMIN' || requestScope.infoCard.username == sessionScope.user.username ||
     requestScope.userFound == false || requestScope.infoCard.role == 'ADMIN'}">
        <div class="panel-flex-end">
            <form class="search-bar" action="${pageContext.request.contextPath}/account" method="GET">
                <input type="hidden" name="command" value="profile">
                <input type="text" name="username" placeholder="<fmt:message key="label.search-user"/>" required
                       title="<fmt:message key="label.valid-username"/>"
                       pattern="^[a-zA-Zа-яА-Я][a-zA-Zа-яА-Я0-9_]{3,20}$"
                >
            </form>
        </div>
    </c:if>
    <c:if test="${requestScope.userFound == false}">
        <h3 class="text-center"><fmt:message key="label.user-not-found"/></h3>
    </c:if>
    <c:if test="${requestScope.userFound == true}">
        <div class="user-card">
            <div class="user-avatar">
                <div class="img-medium">
                    <img src="${pageContext.request.contextPath}/img/avatars/${requestScope.infoCard.imageSrc}"
                         alt="<fmt:message key="label.user-avatar"/>" />
                </div>
            </div>
            <div class="user-info">
                <div class="user-name">
                    <div class="head">
                        <h1>${requestScope.infoCard.name}</h1>
                        <c:if test="${requestScope.infoCard.role == 'USER'}">
                            <h3><fmt:message key="label.user"/></h3>
                        </c:if>
                        <c:if test="${requestScope.infoCard.role == 'ADMIN'}">
                            <h3><fmt:message key="label.admin"/></h3>
                        </c:if>
                        <c:if test="${requestScope.infoCard.role == 'BANNED'}">
                            <h3 class="text-red"><fmt:message key="label.banned"/></h3>
                        </c:if>
                    </div>
                    <h4>${requestScope.infoCard.username}</h4>
                    <h4>${requestScope.infoCard.email}</h4>
                </div>
                <div class="statistics">
                    <div class="win">
                        <h4><fmt:message key="label.win"/></h4>
                        <span class="counter">${requestScope.infoCard.win}</span>
                    </div>
                    <hr>
                    <div class="lose">
                        <h4><fmt:message key="label.lose"/></h4>
                        <span class="counter">${requestScope.infoCard.lose}</span>
                    </div>
                    <hr>
                    <div class="draw">
                        <h4><fmt:message key="label.draw"/></h4>
                        <span class="counter">${requestScope.infoCard.draw}</span>
                    </div>
                </div>
            </div>
        </div>
    </c:if>
</main>
<%@ include file="/jsp/additional/footer.jspf" %>
</body>
</html>