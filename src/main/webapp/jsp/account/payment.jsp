<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="format" uri="/WEB-INF/tld/format" %>
<fmt:setLocale value="${not empty sessionScope['locale'] ? sessionScope['locale'] : 'en_UK'}" scope="session" />
<fmt:setBundle basename="pagecontent" />
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><fmt:message key="title.payment"/></title>
    <title><fmt:message key="label.registration"/></title>
    <link href="<c:url value="/css/header.css" />" rel="stylesheet">
    <link href="<c:url value="/css/main.css" />" rel="stylesheet">
    <link href="<c:url value="/css/payment.css" />" rel="stylesheet">
    <link href="<c:url value="/css/footer.css" />" rel="stylesheet">
</head>
<body>
<%@ include file="/jsp/additional/header.jspf" %>
<main>
    <div class="status-message">
        <h4 class="message"><format:message messageKey="${requestScope.message}"/></h4>
        <h4 class="error-message"><format:message messageKey="${requestScope.errorMessage}"/></h4>
        <c:if test="${requestScope.not_enough_cash == true}">
            <h4 class="error-message"><fmt:message key="label.not-enough-cash"/></h4>
        </c:if>
    </div>
    <h1><fmt:message key="label.payment"/></h1>
    <div class="cash">
        <h3><fmt:message key="label.current-cash"/>${sessionScope.user.cash}</h3>
    </div>
    <div class="payment-box">
        <form action="${pageContext.request.contextPath}/account" method="POST">
            <h3><fmt:message key="label.payment-info"/></h3>
            <select class="payment" name="amount" title="Amount">
                <option>100</option>
                <option>200</option>
                <option>300</option>
                <option>400</option>
                <option>500</option>
            </select>
            <input type="hidden" name="command" value="payment">
            <input type="submit" class="button small" value="<fmt:message key="button.make-payment"/>" />
        </form>
    </div>
</main>
<%@ include file="/jsp/additional/footer.jspf" %>
</body>
</html>