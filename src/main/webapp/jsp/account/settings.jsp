<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="format" uri="/WEB-INF/tld/format" %>
<fmt:setLocale value="${not empty sessionScope['locale'] ? sessionScope['locale'] : 'en_UK'}" scope="session" />
<fmt:setBundle basename="pagecontent" />
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><fmt:message key="title.settings"/></title>
    <link href="<c:url value="/css/header.css" />" rel="stylesheet">
    <link href="<c:url value="/css/main.css" />" rel="stylesheet">
    <link href="<c:url value="/css/settings.css" />" rel="stylesheet">
    <link href="<c:url value="/css/footer.css" />" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="<c:url value="/js/settings.js" />" ></script>
</head>
<body>
<%@ include file="/jsp/additional/header.jspf" %>
<main>
    <div class="status-message">
        <h4 class="message"><format:message messageKey="${requestScope.message}"/></h4>
        <h4 class="error-message"><format:message messageKey="${requestScope.errorMessage}"/></h4>
    </div>
    <h1><fmt:message key="label.account-settings"/></h1>
    <section>
        <h2><fmt:message key="label.profile-avatar"/></h2>
        <form method="POST" action="${pageContext.request.contextPath}/image" name="Avatar" enctype="multipart/form-data">
            <div class="img-large">
                <img src="${pageContext.request.contextPath}/img/avatars/${sessionScope.user.imageSrc}"
                     alt="<fmt:message key="label.user-avatar"/>"
                >
            </div>
            <input type="file" name="avatar" required/>
            <input type="submit" class="button small" name="avatar" value="<fmt:message key="button.save"/>"/>
        </form>
    </section>
    <section>
        <h2><fmt:message key="label.information"/></h2>
        <ul>
            <li>
                <h5><fmt:message key="label.username"/></h5>
                <input class="input" type="text" placeholder="${sessionScope.user.username}" disabled>
            </li>
            <li>
                <h5><fmt:message key="label.email"/></h5>
                <form name="update-email" action="${pageContext.request.contextPath}/account" method="POST" >
                    <input type="email" class="input" id="input-email" name="email" required disabled
                           title="<fmt:message key="label.valid-username"/>"
                           placeholder="${sessionScope.user.email}"
                    >
                    <div class="change-email">
                        <input type="hidden" name="command" value="update_email">
                        <input type="button" class="button small cancel-ok" id="cancel-email"
                               value="<fmt:message key="button.cancel"/>" />
                        <input type="submit" class="button small cancel-ok" id="ok-email"
                               value="<fmt:message key="button.ok"/>" />
                    </div>
                    <input type="button" class="button small" id="change-email" value="<fmt:message key="button.change"/>" />
                </form>
            </li>
            <li>
                <h5><fmt:message key="label.name"/></h5>
                <form name="update-name" action="${pageContext.request.contextPath}/account" method="POST" >
                    <input type="text" class="input" id="input-name" name="name" required disabled
                           title="<fmt:message key="label.valid-username"/>"
                           placeholder="${sessionScope.user.name}"
                    >
                    <div class="change-name">
                        <input type="hidden" name="command" value="update_name">
                        <input type="button" class="button small cancel-ok" id="cancel-name" value="<fmt:message key="button.cancel"/>" />
                        <input type="submit" class="button small cancel-ok" id="ok-name" value="<fmt:message key="button.ok"/>" />
                    </div>
                    <input type="button" class="button small" id="change-name" value="<fmt:message key="button.change"/>" />
                </form>
            </li>
        </ul>
    </section>
    <section>
        <h2><fmt:message key="label.security"/></h2>
        <h5><fmt:message key="label.password"/></h5>
        <form name="update-password" action="${pageContext.request.contextPath}/account" method="POST" >
            <input type="password" class="input" id="input-password" name="current_password" disabled
                   title="<fmt:message key="label.valid-password"/>"
                   placeholder="<fmt:message key="label.current-password"/>"
                   pattern="(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$"
            >
            <input type="button" class="button small" id="change-password" value="<fmt:message key="button.change"/>" />
            <div class="change-password">
                <input type="password" class="input small" id="input-new-password" name="new_password" required
                       title="<fmt:message key="label.valid-password"/>"
                       placeholder="<fmt:message key="label.password"/>"
                       pattern="(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$"
                >
                <input type="password" class="input small" id="input-repeat-new-password" name="new_password_repeat" required
                       title="<fmt:message key="label.valid-password-repeat"/>"
                       placeholder="<fmt:message key="label.password-repeat"/>"
                       pattern="(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$"
                >
                <div>
                    <input type="hidden" name="command" value="update_password">
                    <input type="button" class="button small cancel-ok" id="cancel-password"
                           value="<fmt:message key="button.cancel"/>"
                    >
                    <input type="submit" class="button small cancel-ok" value="<fmt:message key="button.ok"/>" />
                </div>
            </div>
        </form>
    </section>
</main>
<%@ include file="/jsp/additional/footer.jspf" %>
</body>
</html>
