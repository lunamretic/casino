<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${not empty sessionScope['locale'] ? sessionScope['locale'] : 'en_UK'}" scope="session" />
<fmt:setBundle basename="pagecontent" />
<html>
<head>
    <title><fmt:message key="title.error500"/></title>
    <link href="<c:url value="/css/header.css" />" rel="stylesheet">
    <link href="<c:url value="/css/main.css" />" rel="stylesheet">
    <link href="<c:url value="/css/error.css" />" rel="stylesheet">
    <link href="<c:url value="/css/footer.css" />" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="<c:url value="/js/error500.js" />" ></script>
</head>
<body>
<c:choose>
    <c:when test="${sessionScope.user != null}">
        <%@ include file="/jsp/additional/header.jspf" %>
    </c:when>
    <c:otherwise>
        <%@ include file="/jsp/additional/guest_header.jspf" %>
    </c:otherwise>
</c:choose>
<main>
    <img src="<c:url value="/img/error500.jpg"/>" alt="error 500 image" class="error-image" />
    <h1><fmt:message key="label.went-wrong"/></h1>
    <p class="kek"><fmt:message key="label.error-try-again"/></p>
    <input type="submit" class="button" id="show-error" value="<fmt:message key="button.view-error"/>" />
    <div id="error-block">
        <hr>
        <p id="error-message">
            <c:choose>
                <c:when test="${requestScope.errorMessage != null}">
                    <c:out value="${requestScope.errorMessage}"/>
                </c:when>
                <c:otherwise>
                    Request from <c:out value="${pageContext.errorData.requestURI}"/> is failed
                    <br>
                    Servlet name: <c:out value="${pageContext.errorData.servletName}"/>
                    <br>
                    Status code: <c:out value="${pageContext.errorData.statusCode}"/>
                    <br>
                    Exception: <c:out value="${pageContext.exception}"/>
                    <br>
                    Message from exception: <c:out value="${pageContext.exception.message}"/>
                </c:otherwise>
            </c:choose>
        </p>
    </div>
</main>
<%@ include file="/jsp/additional/footer.jspf" %>
</body>
</html>
