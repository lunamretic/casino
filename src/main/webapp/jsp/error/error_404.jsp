<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${not empty sessionScope['locale'] ? sessionScope['locale'] : 'en_UK'}" scope="session" />
<fmt:setBundle basename="pagecontent" />
<html>
<head>
    <title><fmt:message key="title.error404"/></title>
    <link href="<c:url value="/css/header.css" />" rel="stylesheet">
    <link href="<c:url value="/css/main.css" />" rel="stylesheet">
    <link href="<c:url value="/css/error.css" />" rel="stylesheet">
    <link href="<c:url value="/css/footer.css" />" rel="stylesheet">
</head>
<body>
<c:choose>
    <c:when test="${sessionScope.user != null}">
        <%@ include file="/jsp/additional/header.jspf" %>
    </c:when>
    <c:otherwise>
        <%@ include file="/jsp/additional/guest_header.jspf" %>
    </c:otherwise>
</c:choose>
<main>
    <span class="error-404">404</span>
    <h1><fmt:message key="label.page-not-found"/></h1>
    <a href="${pageContext.request.contextPath}/controller?command=news&page=1" class="link">
        <fmt:message key="label.return-news-page"/>
    </a>
</main>
<%@ include file="/jsp/additional/footer.jspf" %>
</body>
</html>
