<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${not empty sessionScope['locale'] ? sessionScope['locale'] : 'en_UK'}" scope="session" />
<fmt:setBundle basename="pagecontent" />
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><fmt:message key="title.search-game"/></title>
    <link href="<c:url value="/css/header.css" />" rel="stylesheet">
    <link href="<c:url value="/css/main.css" />" rel="stylesheet">
    <link href="<c:url value="/css/start_game.css" />" rel="stylesheet">
    <link href="<c:url value="/css/search_wait_bar.css" />" rel="stylesheet">
    <link href="<c:url value="/css/footer.css" />" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="<c:url value="/js/search_game.js" />" ></script>
</head>
<body>
<%@ include file="/jsp/additional/header.jspf" %>
<main>
    <div class="start-game">
        <c:choose>
            <c:when test="${requestScope.banned == true}">
                <h3><fmt:message key="label.account-banned"/></h3>
            </c:when>
            <c:when test="${requestScope.insufficient_funds == true}">
                <h3><fmt:message key="label.insufficient-funds"/></h3>
                <h3><fmt:message key="label.insufficient-funds-message"/></h3>
                <a href="${pageContext.request.contextPath}/jsp/account/payment.jsp" class="link">
                    <fmt:message key="label.go-to-payment"/>
                </a>
            </c:when>
            <c:otherwise>
                <div id="progress-search">
                    <div class="loader"></div>
                    <h2><fmt:message key="label.search-progress"/></h2>
                    <input type="submit" class="button dark" id="cancel-search" value="<fmt:message key="button.cancel-search"/>" />
                </div>
                <div id="start-search">
                    <h2><fmt:message key="label.search-opponent"/></h2>
                    <input type="submit" class="button dark" id="search-opponent" value="<fmt:message key="button.search-opponent"/>" />
                </div>
            </c:otherwise>
        </c:choose>
    </div>
</main>
<%@ include file="/jsp/additional/footer.jspf" %>
</body>
</html>