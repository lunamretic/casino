<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${not empty sessionScope['locale'] ? sessionScope['locale'] : 'en_UK'}" scope="session" />
<fmt:setBundle basename="pagecontent" />
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><fmt:message key="title.start-ai"/></title>
    <link href="<c:url value="/css/header.css" />" rel="stylesheet">
    <link href="<c:url value="/css/main.css" />" rel="stylesheet">
    <link href="<c:url value="/css/start_game.css" />" rel="stylesheet">
    <link href="<c:url value="/css/footer.css" />" rel="stylesheet">
</head>
<body>
<%@ include file="/jsp/additional/header.jspf" %>
<main>
    <div class="start-game">
        <c:choose>
            <c:when test="${requestScope.banned == true}">
                <h3><fmt:message key="label.account-banned"/></h3>
            </c:when>
            <c:when test="${requestScope.insufficient_funds == true}">
                <h3><fmt:message key="label.insufficient-funds"/></h3>
                <h3><fmt:message key="label.insufficient-funds-message"/></h3>
                <a href="${pageContext.request.contextPath}/jsp/account/payment.jsp" class="link">
                    <fmt:message key="label.go-to-payment"/>
                </a>
            </c:when>
            <c:otherwise>
                <h2><fmt:message key="label.start-game-ai"/></h2>
                <form name="start game" action="${pageContext.request.contextPath}/play" method="GET" >
                    <input type="hidden" name="command" value="start_game_ai" />
                    <input type="submit" class="button dark" value="<fmt:message key="label.start"/>" />
                </form>
            </c:otherwise>
        </c:choose>
    </div>
</main>
<%@ include file="/jsp/additional/footer.jspf" %>
</body>
</html>