<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${not empty sessionScope['locale'] ? sessionScope['locale'] : 'en_UK'}" scope="session" />
<fmt:setBundle basename="pagecontent" />
<html>
<head>
    <title><fmt:message key="title.game"/></title>
    <link href="<c:url value="/css/header.css" />" rel="stylesheet">
    <link href="<c:url value="/css/main.css" />" rel="stylesheet">
    <link href="<c:url value="/css/game.css" />" rel="stylesheet">
    <link href="<c:url value="/css/wait_bar.css" />" rel="stylesheet">
    <link href="<c:url value="/css/footer.css" />" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="<c:url value="/js/game.js" />" ></script>
</head>
<body>
<%@ include file="/jsp/additional/header.jspf" %>
<main>
    <div class="opponent-area">
        <div class="opponent-name">
            <h2>${sessionScope.opponentName}</h2>
            <h4><fmt:message key="label.points"/>: <span id="opponent-points">0</span></h4>
        </div>
        <div class="opponent-hand">
            <ul id="opponent-cards">
            </ul>
        </div>
        <div class="info-box">
            <h4><fmt:message key="label.your-cash"/> <span id="cash">${sessionScope.user.cash}</span></h4>
        </div>
    </div>
    <div class="information-area">
        <div class="deck">
            <img src="<c:url value="/img/game/deck.png"/>" alt="deck of cards" class="deck-one">
            <img src="<c:url value="/img/game/deck.png"/>" alt="deck of cards" class="deck-two">
            <img src="<c:url value="/img/game/deck.png"/>" alt="deck of cards" class="deck-three">
        </div>
        <div class="result-box">
            <span id="result-draw" class="result"><fmt:message key="label.result-draw"/></span>
            <span id="result-lose" class="result"><fmt:message key="label.result-lose"/></span>
            <span id="result-win" class="result"><fmt:message key="label.result-win"/></span>
        </div>
        <div class="status-box">
            <h4 id="bet-refused" class="status"><fmt:message key="label.refused-bet"/></h4>
            <h4 id="user-left" class="status"><fmt:message key="label.user-left"/></h4>
            <h4 id="game-ended" class="status"><fmt:message key="label.game-ended"/></h4>
        </div>
        <div id="bet-box">
            <h4><fmt:message key="label.accept-bet-question"/> - <span id="suggested-bet"></span>?</h4>
            <input type="button" class="button small" id="accept-bet" value="<fmt:message key="button.accept"/>" />
            <input type="button" class="button small" id="refuse-bet" value="<fmt:message key="button.refuse"/>" />
        </div>
        <div id="new-game-box">
            <input type="submit" class="button small" id="new-game"  value="<fmt:message key="button.new-game"/>" />
            <input type="submit" class="button small" id="end-game"  value="<fmt:message key="button.end"/>" />
        </div>
        <div id="wait-bar">
            <div class="loader-fb"></div>
        </div>
    </div>
    <div class="user-area">
        <div class="user-name">
            <h4><fmt:message key="label.points"/>: <span id="user-points">0</span></h4>
            <h2>${sessionScope.user.name}</h2>
        </div>
        <div class="user-hand">
            <ul id="user-cards">
            </ul>
        </div>
        <div id="place-bet-box">
            <h3><fmt:message key="label.enter-bet"/></h3>
            <div class="bet-group">
                <input type="button" class="button-bet" id="bet-minus" value="-" disabled />
                <input type="number" class="input-bet" id="bet" value="5" disabled />
                <input type="button" class="button-bet" id="bet-plus" value="+" />
            </div>
            <input type="submit" class="button small" id="place-bet" value="<fmt:message key="button.place-bet"/>" />
        </div>
        <div id="control-box">
            <input type="submit" class="button small" id="draw-card"  value="<fmt:message key="button.draw-card"/>" />
            <input type="submit" class="button small" id="stand" value="<fmt:message key="button.stand"/>" />
        </div>
    </div>
</main>
<%@ include file="/jsp/additional/footer.jspf" %>
</body>
</html>
