<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${not empty sessionScope['locale'] ? sessionScope['locale'] : 'en_UK'}" scope="session" />
<fmt:setBundle basename="pagecontent" />
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><fmt:message key="title.rules"/></title>
    <link href="<c:url value="/css/header.css" />" rel="stylesheet">
    <link href="<c:url value="/css/main.css" />" rel="stylesheet">
    <link href="<c:url value="/css/footer.css" />" rel="stylesheet">
</head>
<body>
<c:choose>
    <c:when test="${sessionScope.user != null}">
        <%@ include file="/jsp/additional/header.jspf" %>
    </c:when>
    <c:otherwise>
        <%@ include file="/jsp/additional/guest_header.jspf" %>
    </c:otherwise>
</c:choose>
<main>
    <h1><fmt:message key="label.game-rules"/></h1>
    <p><fmt:message key="text.game-rules-1"/></p>
    <p><fmt:message key="text.game-rules-2"/></p>
    <h1><fmt:message key="label.site-rules"/></h1>
    <p><fmt:message key="text.site-rules-1"/></p>
    <p><fmt:message key="text.site-rules-2"/></p>
    <p><fmt:message key="text.site-rules-3"/></p>
</main>
<%@ include file="/jsp/additional/footer.jspf" %>
</body>
</html>
