<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="format" uri="/WEB-INF/tld/format" %>
<fmt:setLocale value="${not empty sessionScope['locale'] ? sessionScope['locale'] : 'en_UK'}" scope="session" />
<fmt:setBundle basename="pagecontent" />
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><fmt:message key="title.registration"/></title>
    <link href="<c:url value="/css/header.css" />" rel="stylesheet">
    <link href="<c:url value="/css/main.css" />" rel="stylesheet">
    <link href="<c:url value="/css/authorization.css" />" rel="stylesheet">
    <link href="<c:url value="/css/footer.css" />" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="<c:url value="/js/authorization.js" />" ></script>
</head>
<body>
<%@ include file="/jsp/additional/guest_header.jspf" %>
<main>
    <div class="authorization-form">
        <form name="Register" action="${pageContext.request.contextPath}/controller" method="POST" >
            <h1><fmt:message key="label.registration"/></h1>
            <div class="input-box">
                <span class="required">
                    <input type="text" class="input username" id="username" name="username" required
                           title="<fmt:message key="label.valid-username"/>"
                           placeholder="<fmt:message key="label.username"/>"
                           pattern="^[a-zA-Zа-яА-Я][a-zA-Zа-яА-Я0-9_]{3,20}$"
                    >
                </span>
            </div>
            <div class="invalid-input" id="invalid-username"></div>
            <div class="input-box">
                <span class="required">
                    <input type="email" class="input email" id="email" name="email" required
                           title="<fmt:message key="label.valid-email"/>"
                           placeholder="<fmt:message key="label.email"/>"
                    >
                </span>
            </div>
            <div class="invalid-input" id="invalid-email"></div>
            <div class="input-box">
                <span class="required">
                    <input type="password" class="input password" id="password" name="password"  required
                           title="<fmt:message key="label.valid-password"/>"
                           placeholder="<fmt:message key="label.password"/>"
                           pattern="(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$"
                    >
                </span>
            </div>
            <div class="invalid-input" id="invalid-password"></div>
            <div class="input-box">
                <span class="required">
                    <input type="password" class="input password" id="password-repeat" name="password-repeat" required
                           title="<fmt:message key="label.valid-password-repeat"/>"
                           placeholder="<fmt:message key="label.password-repeat"/>"
                           pattern="(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$"
                    >
                </span>
            </div>
            <div class="invalid-input" id="invalid-password-repeat"></div>
            <input type="hidden" name="command" value="register" />
            <input class="button dark" type="submit" name="register" value="<fmt:message key="label.sign-up"/>" onclick="" />
        </form>
        <div class="error_field">
            <format:message messageKey="${requestScope.errorMessage}"/>
        </div>
    </div>
</main>
<%@ include file="/jsp/additional/footer.jspf" %>
</body>
</html>
