<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="format" uri="/WEB-INF/tld/format" %>
<fmt:setLocale value="${not empty sessionScope['locale'] ? sessionScope['locale'] : 'en_UK'}" scope="session" />
<fmt:setBundle basename="pagecontent" />
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><fmt:message key="title.news"/></title>
    <link href="<c:url value="/css/header.css" />" rel="stylesheet">
    <link href="<c:url value="/css/main.css" />" rel="stylesheet">
    <link href="<c:url value="/css/news.css" />" rel="stylesheet">
    <link href="<c:url value="/css/footer.css" />" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="<c:url value="/js/news.js" />" ></script>
</head>
<body>
<c:choose>
<c:when test="${sessionScope.user != null}">
    <%@ include file="/jsp/additional/header.jspf" %>
</c:when>
<c:otherwise>
    <%@ include file="/jsp/additional/guest_header.jspf" %>
</c:otherwise>
</c:choose>
<main>
    <div class="status-message">
        <c:if test="${requestScope.invalid_form == true}">
            <h4 class="error-message"><fmt:message key="label.invalid-form-news"/></h4>
        </c:if>
    </div>
    <h1><fmt:message key="label.news"/></h1>
    <c:if test="${sessionScope.user.role == 'ADMIN'}">
        <div class="add-news">
            <input type="button" class="button small" id="show-form" value="<fmt:message key="button.add-news"/>">
            <form action="${pageContext.request.contextPath}/controller" method="POST" >
                <div id="new-news">
                    <input type="text" class="headline" name="headline" required
                           title="<fmt:message key="label.valid-headline"/>"
                           placeholder="<fmt:message key="label.headline"/>"
                           pattern="^[\d\D]{4,100}$"
                    >
                    <textarea name="body" class="body" cols="40" rows="4" required
                              placeholder="<fmt:message key="label.input-news"/>"
                    ></textarea>
                    <input type="hidden" name="command" value="news_add">
                    <input type="submit" class="button small" value="<fmt:message key="button.submit"/>">
                </div>
            </form>
        </div>
        <hr class="divider">
    </c:if>
    <ul class="news-list">
        <c:forEach items="${requestScope.newsList}" var="news">
            <li>
                <c:if test="${sessionScope.user.role == 'ADMIN'}">
                    <div class="hide">
                        <form action="${pageContext.request.contextPath}/controller" method="POST" >
                            <input type="hidden" name="command" value="news_remove">
                            <input type="hidden" name="id" value="${news.id}">
                            <input type="submit" class="remove-button" value="<fmt:message key="button.news-remove"/>">
                        </form>
                    </div>
                </c:if>
                <h2>${news.headline}</h2>
                <p>${news.body}</p>
                <div class="news-info">
                    <span class="author">${news.author}</span>
                    <span class="datetime"><fmt:formatDate type="both" value="${news.date}" /></span>
                </div>
            </li>
        </c:forEach>
    </ul>
    <div class="nav-box">
        <div class="nav">
            <input type="hidden" id="has-next-page" value="${requestScope.next_page}">
            <a href="${pageContext.request.contextPath}/controller?command=news&page=${requestScope.page - 1}"
               id="previous">&#8249;</a>
            <span>${requestScope.page}</span>
            <a href="${pageContext.request.contextPath}/controller?command=news&page=${requestScope.page + 1}"
               id="next">&#8250;</a>
        </div>
    </div>
</main>
<%@ include file="/jsp/additional/footer.jspf" %>
</body>
</html>