package com.agarkov.casino.validator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Validator for input forms
 */
public class FormValidator {
    private static final String BODY_REGEX = "^[\\d\\D]{1,10000}$";
    private static final String CASH_REGEX = "\\d+";
    private static final String EMAIL_REGEX = "^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\\.[a-zA-Z0-9-]+)*$";
    private static final String HEADLINE_REGEX = "^[\\d\\D]{4,100}$";
    private static final String NAME_REGEX = "^[a-zA-Zа-яА-Я][a-zA-Zа-яА-Я0-9_]{3,20}$";
    private static final String PASSWORD_REGEX = "(?=^.{8,}$)((?=.*\\d)|(?=.*\\W+))(?![.\\n])(?=.*[A-Z])(?=.*[a-z]).*$";
    private static final String USERNAME_REGEX = "^[a-zA-Zа-яА-Я][a-zA-Zа-яА-Я0-9_]{3,20}$";

    /** Do not instantiate FormValidator. */
    private FormValidator() {
        throw new IllegalAccessError("Utility class");
    }

    /**
     * Validate cash string.
     *
     * @param cash the cash
     * @return error message key if not valid
     */
    public static String validateCash(String cash) {
        String messageKey = null;
        Pattern pattern = Pattern.compile(CASH_REGEX);
        Matcher matcher = pattern.matcher(cash);
        if (!matcher.matches()) {
            messageKey = "message.wrong-cash-amount";
        }
        return messageKey;
    }

    /**
     * Validate email string.
     *
     * @param email the email
     * @return error message key if not valid
     */
    public static String validateEmail(String email) {
        String messageKey = null;
        if (!isValidEmail(email)) {
            messageKey = "message.wrong-email";
        }
        return messageKey;
    }

    /**
     * Validate name string.
     *
     * @param name the name
     * @return error message key if not valid
     */
    public static String validateName(String name) {
        String messageKey = null;
        if (!isValidName(name)) {
            messageKey = "message.wrong-name";
        }
        return messageKey;
    }

    /**
     * Validate news boolean.
     *
     * @param headline the headline
     * @param body     the body
     * @return the boolean
     */
    public static boolean validateNews(String headline, String body) {
        return validateHeadline(headline) && validateBody(body);
    }

    /**
     * Validate password string.
     *
     * @param currentPassword   the current password
     * @param newPassword       the new password
     * @param newPasswordRepeat the new password repeat
     * @return error message key if not valid
     */
    public static String validatePassword(String currentPassword, String newPassword, String newPasswordRepeat) {
        String messageKey = null;
        if (currentPassword == null || !isValidPassword(currentPassword)) {
            messageKey = "message.wrong-password";
        }
        if (messageKey == null && (newPassword == null || !isValidPassword(newPassword))) {
            messageKey = "message.wrong-password";
        }
        if (messageKey == null && (newPasswordRepeat == null || !newPassword.equals(newPasswordRepeat))) {
            messageKey = "message.password-mismatch";
        }
        return messageKey;
    }

    /**
     * Validate authentication string.
     *
     * @param username the username
     * @param password the password
     * @return error message key if not valid
     */
    public static String validateAuthentication(String username, String password) {
        String messageKey = null;
        if (username == null || !isValidUsername(username)) {
            messageKey = "message.wrong-username";
        }
        if (messageKey == null && (password == null || !isValidPassword(password))) {
            messageKey = "message.wrong-password";
        }
        return messageKey;
    }

    /**
     * Validate registration string.
     *
     * @param username       the username
     * @param email          the email
     * @param password       the password
     * @param passwordRepeat the password repeat
     * @return error message key if not valid
     */
    public static String validateRegistration(String username, String email, String password, String passwordRepeat) {
        String messageKey = null;
        if (username == null || !isValidUsername(username)) {
            messageKey = "message.wrong-username";
        }
        if (messageKey == null && (email == null || !isValidEmail(email))) {
            messageKey = "message.wrong-email";
        }
        if (messageKey == null && (password == null || !isValidPassword(password))) {
            messageKey = "message.wrong-password";
        }
        if (messageKey == null && (passwordRepeat == null || !password.equals(passwordRepeat))) {
            messageKey = "message.password-mismatch";
        }
        return messageKey;
    }

    /**
     * Validate username boolean.
     *
     * @param username the username
     * @return the boolean
     */
    public static boolean validateUsername(String username) {
        return isValidUsername(username);
    }

    private static boolean validateBody(String body) {
        Pattern pattern = Pattern.compile(BODY_REGEX);
        Matcher matcher = pattern.matcher(body);
        return matcher.matches();
    }

    private static boolean isValidEmail(String email) {
        Pattern patter = Pattern.compile(EMAIL_REGEX);
        Matcher matcher = patter.matcher(email);
        return matcher.matches();
    }

    private static boolean validateHeadline(String headline) {
        Pattern pattern = Pattern.compile(HEADLINE_REGEX);
        Matcher matcher = pattern.matcher(headline);
        return matcher.matches();
    }

    private static boolean isValidName(String name) {
        Pattern pattern = Pattern.compile(NAME_REGEX);
        Matcher matcher = pattern.matcher(name);
        return matcher.matches();
    }

    private static boolean isValidPassword(String password) {
        Pattern patter = Pattern.compile(PASSWORD_REGEX);
        Matcher matcher = patter.matcher(password);
        return matcher.matches();
    }

    private static boolean isValidUsername(String username) {
        Pattern pattern = Pattern.compile(USERNAME_REGEX);
        Matcher matcher = pattern.matcher(username);
        return matcher.matches();
    }
}