package com.agarkov.casino.util;

import org.apache.commons.codec.digest.DigestUtils;

/**
 * Utility class for encrypting user's password with SHA512 hash function
 */
public class SHA512Util {
    /** Do not instantiate SHA512Util. */
    private SHA512Util() {
        throw new IllegalAccessError("Utility class!");
    }

    /**
     * Encrypt password string.
     *
     * @param username the username
     * @param password the password
     * @return the string
     */
    public static String encryptPassword(String username, String password) {
        return DigestUtils.sha512Hex(DigestUtils.sha512Hex(password) + username.toLowerCase());
    }
}
