package com.agarkov.casino.manager;

import java.util.ResourceBundle;

/**
 * The type Page manager.
 * Manages page keys
 */
public class PageManager {
    private static final ResourceBundle resourceBundle = ResourceBundle.getBundle("page");

    /** Do not instantiate PageManager. */
    private PageManager() {
    }

    /**
     * Gets page from page key
     *
     * @param key the key
     * @return the page
     */
    public static String getPage(String key) {
        return resourceBundle.getString(key);
    }
}
