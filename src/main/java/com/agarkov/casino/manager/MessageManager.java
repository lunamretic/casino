package com.agarkov.casino.manager;

import java.util.Locale;
import java.util.ResourceBundle;

/**
 * The enum Message manager.
 * Manages message keys according to user's locale
 */
public enum MessageManager {
    /**
     * Russian locale
     */
    RU(ResourceBundle.getBundle("pagecontent", new Locale("ru", "RU"))),
    /**
     * English locale
     */
    EN(ResourceBundle.getBundle("pagecontent", new Locale("en", "UK")));

    private ResourceBundle bundle;

    MessageManager(ResourceBundle bundle) {
        this.bundle = bundle;
    }

    /**
     * Gets message text from message key
     *
     * @param key the key
     * @return the message
     */
    public String getMessage(String key) {
        return bundle.getString(key);
    }
}