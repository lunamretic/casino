package com.agarkov.casino.filter;

import com.agarkov.casino.entity.User;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * The type Cash filter.
 * Prohibits users with an empty cash from accessing the game pages
 */
@WebFilter(urlPatterns = {"/play"})
public class CashFilter implements Filter {
    private static final String ATTR_INSUFFICIENT_FUNDS = "insufficient_funds";
    private static final String ATTR_USER = "user";
    private static final int MIN_CASH = 30;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest httpRequest = (HttpServletRequest) request;
        HttpSession session = httpRequest.getSession(false);
        User user = (User) session.getAttribute(ATTR_USER);
        if (user != null && user.getCash() < MIN_CASH) {
            httpRequest.setAttribute(ATTR_INSUFFICIENT_FUNDS, true);
        }
        chain.doFilter(request, response);
    }

    @Override
    public void destroy() {

    }
}