package com.agarkov.casino.filter;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * The type Authorization filter.
 * Prohibits unauthorized users from accessing the game and account pages
 */
@WebFilter(urlPatterns = {"/account", "/game", "/image", "/play"})
public class AuthorizationFilter implements Filter {
    private static final String ATTR_USER = "user";

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest httpRequest = (HttpServletRequest) request;
        HttpServletResponse httpResponse = (HttpServletResponse) response;
        HttpSession session = httpRequest.getSession(false);
        if (session.getAttribute(ATTR_USER) == null) {
            httpResponse.sendRedirect("/authorization?command=page&page=log-in");
        } else {
            chain.doFilter(request, response);
        }
    }

    @Override
    public void destroy() {

    }
}
