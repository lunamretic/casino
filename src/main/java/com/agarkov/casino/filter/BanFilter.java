package com.agarkov.casino.filter;

import com.agarkov.casino.entity.Role;
import com.agarkov.casino.entity.User;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * The type Ban filter.
 * Prohibits banned users from accessing the game pages
 */
@WebFilter(urlPatterns = {"/play"})
public class BanFilter implements Filter {
    private static final String ATTR_BANNED = "banned";
    private static final String ATTR_USER = "user";

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest httpRequest = (HttpServletRequest) request;
        HttpSession session = httpRequest.getSession(false);
        User user = (User) session.getAttribute(ATTR_USER);
        if (user != null && Role.BANNED == user.getRole()) {
            httpRequest.setAttribute(ATTR_BANNED, true);
        }
        chain.doFilter(request, response);
    }

    @Override
    public void destroy() {

    }
}