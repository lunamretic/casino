package com.agarkov.casino.filter;

import com.agarkov.casino.manager.PageManager;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * The type User filter.
 * Prohibits unauthorized users from accessing the authorization pages
 */
@WebFilter(urlPatterns = {"/authorization"})
public class UserFilter implements Filter {
    private static final String ATTR_USER = "user";

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest httpRequest = (HttpServletRequest) request;
        HttpSession session = httpRequest.getSession(false);
        if (session.getAttribute(ATTR_USER) != null) {
            httpRequest.getRequestDispatcher(PageManager.getPage("index")).forward(request, response);
        } else {
            chain.doFilter(request, response);
        }
    }

    @Override
    public void destroy() {

    }
}
