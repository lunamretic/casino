package com.agarkov.casino.exception;

/**
 * The type Game session exception.
 */
public class GameSessionException extends Exception {
    /**
     * Instantiates a new Game session exception.
     */
    public GameSessionException() {
    }

    /**
     * Instantiates a new Game session exception.
     *
     * @param message the message
     */
    public GameSessionException(String message) {
        super(message);
    }

    /**
     * Instantiates a new Game session exception.
     *
     * @param message the message
     * @param cause   the cause
     */
    public GameSessionException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * Instantiates a new Game session exception.
     *
     * @param cause the cause
     */
    public GameSessionException(Throwable cause) {
        super(cause);
    }
}
