package com.agarkov.casino.pool;

import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

/**
 * The type Connector database.
 */
public class ConnectorDB {
    private static final Logger LOG = Logger.getLogger(ConnectorDB.class);

    /**
     * Gets connection.
     *
     * @return the connection
     * @throws SQLException the sql exception
     */
    public static Connection getConnection() throws SQLException {
        String url = null;
        String user = null;
        String pass = null;
        try {
            ResourceBundle resource = ResourceBundle.getBundle("database");
            url = resource.getString("db.url");
            user = resource.getString("db.user");
            pass = resource.getString("db.password");
        } catch (MissingResourceException e) {
            LOG.fatal("Invalid properties for database", e);
        }
        return DriverManager.getConnection(url.concat("?useSSL=false"), user, pass);
    }
}
