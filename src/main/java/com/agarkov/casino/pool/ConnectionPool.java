package com.agarkov.casino.pool;

import com.agarkov.casino.exception.PoolException;
import com.mysql.jdbc.Driver;
import org.apache.log4j.Logger;

import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * The type Connection pool.
 */
public class ConnectionPool {
    private static final Logger LOG = Logger.getLogger(ConnectionPool.class);

    private static final int POOL_SIZE = 5;
    private static final int ATTEMPTS_LIMIT = 5;
    private static ConnectionPool connectionPool;
    private static ArrayBlockingQueue<ProxyConnection> connectionQueue;
    private static Lock lock = new ReentrantLock();
    private static AtomicBoolean instanceCreated = new AtomicBoolean();


    private ConnectionPool() throws PoolException {
        LOG.debug("Connecting to db...");

        try {
            DriverManager.registerDriver(new Driver());
        } catch (SQLException e) {
            throw new PoolException("Can't register JDBC driver! ", e);
        }

        int currentPoolSize = 0;
        int attemptsCount = 0;
        connectionQueue = new ArrayBlockingQueue<>(POOL_SIZE);
        while (currentPoolSize < POOL_SIZE && attemptsCount < ATTEMPTS_LIMIT) {
            try {
                ProxyConnection proxyConnection = new ProxyConnection(ConnectorDB.getConnection());
                connectionQueue.offer(proxyConnection);
                currentPoolSize++;
            } catch (SQLException e) {
                attemptsCount++;
                LOG.warn("Can't get connection", e);
            }
        }
        if (attemptsCount == ATTEMPTS_LIMIT) {
            throw new PoolException("Exceeded the maximum number of attempts");
        }
    }

    /**
     * Gets instance.
     *
     * @return the instance
     */
    public static ConnectionPool getInstance() {
        if (!instanceCreated.get()) {
            lock.lock();
            try {
                if (connectionPool == null) {
                    connectionPool = new ConnectionPool();
                    instanceCreated.set(true);
                    LOG.info("Pool created.");
                }
            } catch (PoolException e) {
                LOG.fatal("Can't create connection pool", e);
                throw new RuntimeException("Can't create connection pool! ", e);
            } finally {
                lock.unlock();
            }
        }
        return connectionPool;
    }

    /**
     * Gets connection.
     *
     * @return the connection
     * @throws PoolException the pool exception
     */
    public ProxyConnection getConnection() throws PoolException {
        ProxyConnection connection;
        try {
            connection = connectionQueue.take();
        } catch (InterruptedException e) {
            throw new PoolException("Can't get connection from queue!", e);
        }
        return connection;
    }

    /**
     * Return connection.
     *
     * @param connection the connection
     */
    public static void returnConnection(ProxyConnection connection) {
        connectionQueue.offer(connection);
    }

    /**
     * Close pool.
     */
    public static void closePool() {
        int attemptsCount = 0;
        while (!connectionQueue.isEmpty() && attemptsCount < ATTEMPTS_LIMIT) {
            try {
                connectionQueue.take().closeConnection();
            } catch (SQLException e) {
                attemptsCount++;
                LOG.warn("Can't close connection", e);
            } catch (InterruptedException e) {
                LOG.error("Can't close pool", e);
            }
        }
        if (!connectionQueue.isEmpty()) {
            LOG.error("Can't close pool");
        }
    }

}