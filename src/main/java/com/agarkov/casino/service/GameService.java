package com.agarkov.casino.service;

import com.agarkov.casino.dao.GameSessionDAO;
import com.agarkov.casino.dao.UserDAO;
import com.agarkov.casino.entity.game.Card;
import com.agarkov.casino.entity.game.Player;
import com.agarkov.casino.entity.game.response.DefineTurnResponse;
import com.agarkov.casino.entity.game.response.DrawCardResponse;
import com.agarkov.casino.entity.game.response.FinishGameResponse;
import com.agarkov.casino.entity.game.response.GameSearchResponse;
import com.agarkov.casino.entity.game.response.ServerResponse;
import com.agarkov.casino.entity.game.response.StandResponse;
import com.agarkov.casino.entity.game.response.StartHandResponse;
import com.agarkov.casino.exception.DAOException;
import com.agarkov.casino.exception.GameSessionException;
import com.agarkov.casino.exception.ServiceException;
import com.agarkov.casino.logic.GameSession;

import java.util.ArrayList;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ConcurrentHashMap;

/**
 * The type Game service.
 */
public class GameService {
    private static final String OPPONENT_AI = "AI";
    private static GameService gameService = null;
    private static ArrayBlockingQueue<Player> awaitingPlayers;
    private static ConcurrentHashMap<Long, GameSession> gameSessions;
    private static ConcurrentHashMap<Long, Long> playerGameMap;

    /** Do not instantiate GameService. */
    private GameService() {
        awaitingPlayers = new ArrayBlockingQueue<>(10, true);
        gameSessions = new ConcurrentHashMap<>();
        playerGameMap = new ConcurrentHashMap<>();
    }

    /**
     * Gets instance.
     *
     * @return the instance
     */
    public static GameService getInstance() {
        if (gameService == null) {
            gameService = new GameService();
        }
        return gameService;
    }

    /**
     * Search game game search response.
     *
     * @param userId   the user id
     * @param username the username
     * @param cash     the cash
     * @return the game search response
     */
    public GameSearchResponse searchGame(long userId, String username, int cash) {
        Player player = new Player();
        player.setId(userId);
        player.setUsername(username);
        player.setCash(cash);

        GameSearchResponse gameSearchResponse = null;
        Player player2 = awaitingPlayers.poll();
        if (player2 == null) {
            player.setNumber(1);
            awaitingPlayers.offer(player);
        } else {
            player.setNumber(2);
            long gameId = startGame(player2, player);
            gameSearchResponse = new GameSearchResponse();
            gameSearchResponse.setGameId(gameId);
            gameSearchResponse.setOpponentName(player2.getUsername());

            playerGameMap.put(player2.getId(), gameId);
        }
        return gameSearchResponse;
    }

    /**
     * Wait player game search response.
     *
     * @param userId the user id
     * @return the game search response
     * @throws ServiceException the service exception
     */
    public GameSearchResponse waitPlayer(long userId) throws ServiceException {
        GameSearchResponse gameSearchResponse = null;
        if (playerGameMap.containsKey(userId)) {
            long gameId = playerGameMap.remove(userId);
            GameSession gameSession = gameSessions.get(gameId);
            String opponentName;
            try {
                opponentName = gameSession.getOpponentName(userId);
            } catch (GameSessionException e) {
                throw new ServiceException(e);
            }
            gameSearchResponse = new GameSearchResponse();
            gameSearchResponse.setGameId(gameId);
            gameSearchResponse.setOpponentName(opponentName);
        }
        return gameSearchResponse;
    }

    /**
     * Cancel search.
     *
     * @param userId the user id
     */
    public void cancelSearch(long userId) {
        awaitingPlayers.removeIf(player -> player.getId() == userId);
    }

    private long startGame(Player player1, Player player2) {
        long gameId = System.currentTimeMillis();
        GameSession gameSession = new GameSession(gameId, player1, player2, false);
        gameSessions.put(gameId, gameSession);
        return gameId;
    }

    /**
     * Start game ai long.
     *
     * @param id   the id
     * @param name the name
     * @param cash the cash
     * @return the long
     */
    public long startGameAI(long id, String name, int cash) {
        long gameId = System.currentTimeMillis();

        Player player1 = new Player();
        player1.setId(id);
        player1.setUsername(name);
        player1.setNumber(1);
        player1.setCash(cash);

        Player player2 = new Player();
        player2.setId(0);
        player2.setUsername(OPPONENT_AI);
        player2.setNumber(2);

        GameSession gameSession = new GameSession(gameId, player1, player2, true);

        gameSessions.put(gameId, gameSession);

        return gameId;
    }

    /**
     * Define turn server response.
     *
     * @param gameId the game id
     * @param userId the user id
     * @return the server response
     * @throws ServiceException the service exception
     */
    public ServerResponse defineTurn(long gameId, long userId) throws ServiceException {
        GameSession gameSession = gameSessions.get(gameId);
        DefineTurnResponse serverResponse;
        try {
            boolean isPlayerTurn = gameSession.defineTurn(userId);
            serverResponse = new DefineTurnResponse();
            serverResponse.setPlayerTurn(isPlayerTurn);
            serverResponse.setAI(gameSession.isAI());
        } catch (GameSessionException e) {
            throw new ServiceException(e);
        }
        return serverResponse;
    }

    /**
     * Place bet.
     *
     * @param gameId the game id
     * @param userId the user id
     * @param bet    the bet
     * @throws ServiceException the service exception
     */
    public void placeBet(long gameId, long userId, int bet) throws ServiceException {
        GameSession gameSession = gameSessions.get(gameId);
        try {
            gameSession.placeBet(userId, bet);
        } catch (GameSessionException e) {
            throw new ServiceException(e);
        }
    }

    /**
     * Accept bet.
     *
     * @param gameId the game id
     * @param userId the user id
     * @throws ServiceException the service exception
     */
    public void acceptBet(long gameId, long userId) throws ServiceException {
        GameSession gameSession = gameSessions.get(gameId);
        try {
            gameSession.acceptBet(userId);
        } catch (GameSessionException e) {
            throw new ServiceException(e);
        }
    }

    /**
     * Refuse bet.
     *
     * @param gameId the game id
     * @param userId the user id
     * @throws ServiceException the service exception
     */
    public void refuseBet(long gameId, long userId) throws ServiceException {
        GameSession gameSession = gameSessions.get(gameId);
        try {
            gameSession.refuseBet(userId);
        } catch (GameSessionException e) {
            throw new ServiceException(e);
        }
    }

    /**
     * Receive start hand server response.
     *
     * @param gameId the game id
     * @param userId the user id
     * @return the server response
     * @throws ServiceException the service exception
     */
    public ServerResponse receiveStartHand(long gameId, long userId) throws ServiceException {
        GameSession gameSession = gameSessions.get(gameId);
        StartHandResponse serverResponse;
        try {
            ArrayList<Card> startHand = gameSession.receiveStartHand(userId);
            serverResponse = new StartHandResponse();
            serverResponse.setStartHand(startHand);
            serverResponse.setPoints(gameSession.getPlayerPoints(userId));
        } catch (GameSessionException e) {
            throw new ServiceException(e);
        }

        return serverResponse;
    }

    /**
     * Polling server response.
     * Asks server if there are some changes in current game.
     *
     * @param gameId the game id
     * @param userId the user id
     * @return the server response
     * @throws ServiceException the service exception
     */
    public ServerResponse polling(long gameId, long userId) throws ServiceException {
        GameSession gameSession = gameSessions.get(gameId);
        ServerResponse serverResponse;

        try {
            serverResponse = gameSession.polling(userId);
            if (serverResponse != null && serverResponse.getClass() == FinishGameResponse.class) {
                finishGame(gameId);
            }
        } catch (GameSessionException e) {
            throw new ServiceException(e);
        }

        return serverResponse;
    }

    /**
     * Draw card server response.
     *
     * @param gameId the game id
     * @param userId the user id
     * @return the server response
     * @throws ServiceException the service exception
     */
    public ServerResponse drawCard(long gameId, long userId) throws ServiceException {
        GameSession gameSession = gameSessions.get(gameId);
        DrawCardResponse serverResponse;
        Card card;
        if (gameSession != null) {
            try {
                serverResponse = new DrawCardResponse();
                card = gameSession.drawCard(userId);
                int points = gameSession.getPlayerPoints(userId);
                serverResponse.setCard(card);
                serverResponse.setPoints(points);
            } catch (GameSessionException e) {
                throw new ServiceException(e);
            }
        } else {
            throw new ServiceException("Invalid game session!");
        }

        return serverResponse;
    }

    /**
     * Stand stand response.
     * Ends player's turn.
     *
     * @param gameId the game id
     * @param userId the user id
     * @return the stand response
     * @throws ServiceException the service exception
     */
    public StandResponse stand(long gameId, long userId) throws ServiceException {
        GameSession gameSession = gameSessions.get(gameId);
        StandResponse serverResponse = null;
        try {
            boolean isSecondPlayerStand;
            isSecondPlayerStand = gameSession.stand(userId);

            if (isSecondPlayerStand) {
                String result = gameSession.determineResults(userId);
                gameSession.updateCash();
                int opponentPoint = gameSession.getOpponentPoints(userId);
                int cash = gameSession.getPlayer2().getCash();
                ArrayList<Card> opponentHand = gameSession.getOpponentHand(userId);

                serverResponse = new StandResponse();
                serverResponse.setMessage(result);
                serverResponse.setOpponentPoints(opponentPoint);
                serverResponse.setOpponentHand(opponentHand);
                serverResponse.setCash(cash);
            }
        } catch (GameSessionException e) {
            throw new ServiceException(e);
        }

        return serverResponse;
    }

    /**
     * Stand ai stand response.
     * AI turn.
     * Saves results of the game.
     *
     * @param gameId the game id
     * @param userId the user id
     * @return the stand response
     * @throws ServiceException the service exception
     */
    public StandResponse standAI(long gameId, long userId) throws ServiceException {
        GameSession gameSession = gameSessions.get(gameId);
        StandResponse serverResponse;
        try {
            serverResponse = new StandResponse();
            String result = gameSession.standAI(userId);
            gameSession.updateCash();
            int cash = gameSession.getPlayer1().getCash();

            serverResponse.setMessage(result);
            serverResponse.setOpponentPoints(gameSession.getPlayer2().getPoints());
            serverResponse.setOpponentHand(gameSession.getPlayer2().getHand());
            serverResponse.setCash(cash);
            saveResults(gameId);
            gameSession.setSaved(true);
        } catch (GameSessionException e) {
            throw new ServiceException(e);
        }

        return serverResponse;
    }

    /**
     * Offer new game.
     *
     * @param gameId the game id
     * @param userId the user id
     * @throws ServiceException the service exception
     */
    public void offerNewGame(long gameId, long userId) throws ServiceException {
        GameSession gameSession = gameSessions.get(gameId);
        try {
            if (!gameSession.isAI()) {
                if (!gameSession.isSaved()) {
                    saveResults(gameId);
                    gameSession.setSaved(true);
                }
                gameSession.offerNewGame(userId);
            } else {
                gameSession.newGame();
            }
        } catch (GameSessionException e) {
            throw new ServiceException(e);
        }
    }


    /**
     * Leave game int.
     * Determines the leave condition and save the game according to it.
     *
     * @param gameId the game id
     * @param userId the user id
     * @return the int
     * @throws ServiceException the service exception
     */
    public int leaveGame(long gameId, long userId) throws ServiceException {
        GameSession gameSession = gameSessions.get(gameId);
        int cash = -1;

        if (gameSession != null) {
            try {
                String leaveCondition = gameSession.leaveGame(userId);
                if (!gameSession.isSaved() && !"bet-refused".equals(leaveCondition)) {
                    gameSession.updateCash();
                    saveResults(gameId);
                    gameSession.setSaved(true);
                }

                cash = gameSession.getPlayerCash(userId);

                if (gameSession.isAI()) {
                    finishGame(gameId);
                } else {
                    gameSession.informLeave(userId, leaveCondition);
                }
            } catch (GameSessionException e) {
                throw new ServiceException(e);
            }
        }
        return cash;
    }

    private void finishGame(long gameId) throws ServiceException {
        gameSessions.remove(gameId);
    }

    private void saveResults(long gameId) throws ServiceException {
        GameSession gameSession = gameSessions.get(gameId);
        long player1 = gameSession.getPlayer1().getId();
        long player2 = gameSession.getPlayer2().getId();
        int winner = gameSession.getWinner();
        boolean isAI = gameSession.isAI();
        int bet = gameSession.getBet();

        GameSessionDAO gameSessionDAO = new GameSessionDAO();
        UserDAO userDAO = new UserDAO();
        try {
            gameSessionDAO.add(gameId, player1, player2, winner, isAI, bet);

            if (winner < 0) {
                userDAO.depositCash(player1, String.valueOf(bet));
                userDAO.withdrawCash(player2, String.valueOf(bet));
            } else {
                if (winner > 0) {
                    userDAO.withdrawCash(player1, String.valueOf(bet));
                    userDAO.depositCash(player2, String.valueOf(bet));
                }
            }
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }
}
