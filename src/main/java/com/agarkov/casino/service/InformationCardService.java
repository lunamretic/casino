package com.agarkov.casino.service;

import com.agarkov.casino.dao.InformationCardDAO;
import com.agarkov.casino.entity.InformationCard;
import com.agarkov.casino.exception.DAOException;
import com.agarkov.casino.exception.ServiceException;
import com.agarkov.casino.service.response.ProfileResponse;
import com.agarkov.casino.validator.FormValidator;

import java.util.List;

/**
 * The type Information card service.
 */
public class InformationCardService {
    private static InformationCardService informationCardService = null;

    /** Do not instantiate InformationCardService. */
    private InformationCardService() {
    }

    /**
     * Gets instance.
     *
     * @return the instance
     */
    public static InformationCardService getInstance() {
        if (informationCardService == null) {
            informationCardService = new InformationCardService();
        }
        return informationCardService;
    }

    /**
     * Find top players list.
     * List of top 10 players by winrate percent.
     *
     * @return the list
     * @throws ServiceException the service exception
     */
    public List<InformationCard> findTopPlayers() throws ServiceException {
        InformationCardDAO informationCardDAO = new InformationCardDAO();
        List<InformationCard> topPlayers;

        try {
            topPlayers = informationCardDAO.findTopPlayers();
        } catch (DAOException e) {
            throw new ServiceException(e);
        }

        return topPlayers;
    }

    /**
     * Find user profile response.
     * User's public information.
     *
     * @param username the username
     * @return the profile response
     * @throws ServiceException the service exception
     */
    public ProfileResponse findUser(String username) throws ServiceException {
        ProfileResponse profileResponse = new ProfileResponse();

        if (FormValidator.validateUsername(username)) {
            profileResponse.setValid(true);
        } else {
            profileResponse.setValid(false);
            return profileResponse;
        }

        InformationCardDAO informationCardDAO = new InformationCardDAO();
        InformationCard informationCard;

        try {
            informationCard = informationCardDAO.findPlayer(username);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }

        profileResponse.setInformationCard(informationCard);

        return profileResponse;
    }
}
