package com.agarkov.casino.service;

import com.agarkov.casino.dao.UserDAO;
import com.agarkov.casino.entity.Role;
import com.agarkov.casino.entity.User;
import com.agarkov.casino.exception.DAOException;
import com.agarkov.casino.exception.ServiceException;
import com.agarkov.casino.service.response.RegisterResponse;
import com.agarkov.casino.util.SHA512Util;
import com.agarkov.casino.validator.FormValidator;

/**
 * The type User service.
 */
public class UserService {
    private static UserService userService = null;

    /** Do not instantiate UserService. */
    private UserService() {
    }

    /**
     * Gets instance.
     *
     * @return the instance
     */
    public static UserService getInstance() {
        if (userService == null) {
            userService = new UserService();
        }
        return userService;
    }

    /**
     * Update email string.
     *
     * @param id       the id
     * @param newEmail the new email
     * @return the string
     * @throws ServiceException the service exception
     */
    public String updateEmail(long id, String newEmail) throws ServiceException {
        String errorMessage;
        errorMessage = FormValidator.validateEmail(newEmail);

        if (errorMessage != null) {
            return errorMessage;
        }
        UserDAO userDAO = new UserDAO();
        try {
            userDAO.updateEmail(id, newEmail);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
        return null;
    }

    /**
     * Update name string.
     *
     * @param id      the id
     * @param newName the new name
     * @return the string
     * @throws ServiceException the service exception
     */
    public String updateName(long id, String newName) throws ServiceException {
        String errorMessage;
        errorMessage = FormValidator.validateName(newName);

        if (errorMessage != null) {
            return errorMessage;
        }
        UserDAO userDAO = new UserDAO();
        try {
            userDAO.updateName(id, newName);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
        return null;
    }

    /**
     * Update password string.
     *
     * @param id                the id
     * @param currentPassword   the current password
     * @param newPassword       the new password
     * @param newPasswordRepeat the new password repeat
     * @param username          the username
     * @return the string
     * @throws ServiceException the service exception
     */
    public String updatePassword(long id, String currentPassword, String newPassword, String newPasswordRepeat, String username) throws ServiceException {
        String errorMessage;
        errorMessage = FormValidator.validatePassword(currentPassword, newPassword, newPasswordRepeat);
        if (errorMessage != null) {
            return errorMessage;
        }
        String encryptedCurrentPassword = SHA512Util.encryptPassword(username, currentPassword);
        String encryptedNewPassword = SHA512Util.encryptPassword(username, newPassword);
        UserDAO userDAO = new UserDAO();
        try {
            boolean isUpdated = userDAO.updatePassword(id, encryptedCurrentPassword, encryptedNewPassword);
            if (!isUpdated) {
                return "message.password-incorrect";
            }
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
        return null;
    }

    /**
     * Update avatar.
     *
     * @param id       the id
     * @param filePath the file path
     * @throws ServiceException the service exception
     */
    public void updateAvatar(long id, String filePath) throws ServiceException {
        UserDAO userDAO = new UserDAO();
        try {
            userDAO.updateAvatar(id, filePath);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    /**
     * Find cash int.
     *
     * @param id the id
     * @return the int
     * @throws ServiceException the service exception
     */
    public int findCash(long id) throws ServiceException {
        UserDAO userDAO = new UserDAO();
        int cash;

        try {
            cash = userDAO.findCash(id);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }

        return cash;
    }

    /**
     * Deposit cash string.
     *
     * @param id   the id
     * @param cash the cash
     * @return the string
     * @throws ServiceException the service exception
     */
    public String depositCash(long id, String cash) throws ServiceException {
        String errorMessage;
        errorMessage = FormValidator.validateCash(cash);
        if (errorMessage != null) {
            return errorMessage;
        }
        UserDAO userDAO = new UserDAO();
        try {
            userDAO.depositCash(id, cash);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
        return null;
    }

    /**
     * Withdraw cash string.
     *
     * @param id   the id
     * @param cash the cash
     * @return the string
     * @throws ServiceException the service exception
     */
    public String withdrawCash(long id, String cash) throws ServiceException {
        String errorMessage;
        errorMessage = FormValidator.validateCash(cash);
        if (errorMessage != null) {
            return errorMessage;
        }
        UserDAO userDAO = new UserDAO();
        try {
            userDAO.withdrawCash(id, cash);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
        return null;
    }

    /**
     * Update role.
     *
     * @param role     the role
     * @param username the username
     * @throws ServiceException the service exception
     */
    public void updateRole(Role role, String username) throws ServiceException {
        UserDAO userDAO = new UserDAO();

        try {
            userDAO.updateRole(role, username);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    /**
     * Log in user.
     *
     * @param username the username
     * @param password the password
     * @return the user
     * @throws ServiceException the service exception
     */
    public User logIn(String username, String password) throws ServiceException {
        UserDAO userDAO = new UserDAO();
        User user;

        try {
            user = userDAO.findByAuthentication(username, SHA512Util.encryptPassword(username, password));
        } catch (DAOException e) {
            throw new ServiceException(e);
        }

        return user;
    }

    /**
     * Register register response.
     *
     * @param username       the username
     * @param email          the email
     * @param password       the password
     * @param passwordRepeat the password repeat
     * @return the register response
     * @throws ServiceException the service exception
     */
    public RegisterResponse register(String username, String email, String password, String passwordRepeat) throws ServiceException {
        RegisterResponse registerResponse = new RegisterResponse();
        String errorMessage;
        errorMessage = FormValidator.validateRegistration(username, email, password, passwordRepeat);

        if (errorMessage != null) {
            registerResponse.setErrorMessage(errorMessage);
            return registerResponse;
        }

        UserDAO userDAO = new UserDAO();
        long id;
        try {
            boolean isAlreadyTaken = userDAO.checkExistence(username, email);

            if (!isAlreadyTaken) {
                User user = new User();
                user.setEmail(email);
                user.setUsername(username);
                user.setName(username);
                String encryptedPassword = SHA512Util.encryptPassword(username, password);
                id = userDAO.add(user, encryptedPassword);
                user.setId(id);
                registerResponse.setUser(user);
            } else {
                registerResponse.setErrorMessage("message.username-email-taken");
            }
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
        return registerResponse;
    }
}
