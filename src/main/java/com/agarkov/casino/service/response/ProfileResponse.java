package com.agarkov.casino.service.response;

import com.agarkov.casino.entity.InformationCard;

/**
 * The type Profile response.
 */
public class ProfileResponse {
    private InformationCard informationCard;
    private boolean isValid;

    /**
     * Gets information card.
     *
     * @return the information card
     */
    public InformationCard getInformationCard() {
        return informationCard;
    }

    /**
     * Sets information card.
     *
     * @param informationCard the information card
     */
    public void setInformationCard(InformationCard informationCard) {
        this.informationCard = informationCard;
    }

    /**
     * Is valid boolean.
     *
     * @return the boolean
     */
    public boolean isValid() {
        return isValid;
    }

    /**
     * Sets valid.
     *
     * @param valid the valid
     */
    public void setValid(boolean valid) {
        isValid = valid;
    }
}
