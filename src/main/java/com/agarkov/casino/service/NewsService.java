package com.agarkov.casino.service;

import com.agarkov.casino.dao.NewsDAO;
import com.agarkov.casino.entity.News;
import com.agarkov.casino.exception.DAOException;
import com.agarkov.casino.exception.ServiceException;
import com.agarkov.casino.validator.FormValidator;

import java.util.Date;
import java.util.List;

/**
 * The type News service.
 */
public class NewsService {
    private static NewsService newsService = null;

    /** Do not instantiate NewsService. */
    private NewsService() {
    }

    /**
     * Gets instance.
     *
     * @return the instance
     */
    public static NewsService getInstance() {
        if (newsService == null) {
            newsService = new NewsService();
        }
        return newsService;
    }

    /**
     * Add news boolean.
     *
     * @param id       the id
     * @param headline the headline
     * @param body     the body
     * @return the boolean
     * @throws ServiceException the service exception
     */
    public boolean add(long id, String headline, String body) throws ServiceException {
        if (!FormValidator.validateNews(headline, body)) {
            return false;
        }

        NewsDAO newsDAO = new NewsDAO();

        News news = new News();
        news.setHeadline(headline);
        news.setBody(body);
        news.setDate(new Date());

        try {
            newsDAO.add(id, news);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }

        return true;
    }

    /**
     * Load page of news list.
     *
     * @param page the page
     * @return the list
     * @throws ServiceException the service exception
     */
    public List<News> loadPage(int page) throws ServiceException {
        NewsDAO newsDAO = new NewsDAO();
        List<News> newsList;

        try {
            newsList = newsDAO.loadPage(page);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }

        return newsList;
    }

    /**
     * Remove news.
     *
     * @param id the id
     * @throws ServiceException the service exception
     */
    public void remove(long id) throws ServiceException {
        NewsDAO newsDAO = new NewsDAO();

        try {
            newsDAO.remove(id);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }
}
