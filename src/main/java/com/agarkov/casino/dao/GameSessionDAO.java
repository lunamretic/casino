package com.agarkov.casino.dao;

import com.agarkov.casino.exception.DAOException;
import com.agarkov.casino.exception.PoolException;
import com.agarkov.casino.logic.GameSession;
import com.agarkov.casino.pool.ConnectionPool;
import com.agarkov.casino.pool.ProxyConnection;
import org.apache.log4j.Logger;

import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * The type Game session dao.
 */
public class GameSessionDAO extends AbstractDAO<GameSession> {
    private static final Logger LOG = Logger.getLogger(GameSessionDAO.class);

    private static final String SQL_INSERT_GAME = "INSERT INTO game " +
            "(session, player1, player2, winner, ai, bet) " +
            "VALUES (?, ?, ?, ?, ?, ?)";

    /**
     * Add new game session to database
     *
     * @param gameSession the game session
     * @param user1       the user 1
     * @param user2       the user 2
     * @param winner      the winner
     * @param ai          the ai
     * @param bet         the bet
     * @throws DAOException the dao exception
     */
    public void add(long gameSession, long user1, long user2, int winner, boolean ai, int bet) throws DAOException {
        ProxyConnection connection;
        try {
            connection = ConnectionPool.getInstance().getConnection();
        } catch (PoolException e) {
            throw new DAOException(e);
        }

        try (PreparedStatement statement = connection.prepareStatement(SQL_INSERT_GAME)) {
            connection.setAutoCommit(false);
            statement.setLong(1, gameSession);
            statement.setLong(2, user1);
            statement.setLong(3, user2);
            statement.setInt(4, winner);
            statement.setBoolean(5, ai);
            statement.setInt(6, bet);
            statement.executeUpdate();
            connection.commit();
        } catch (SQLException e) {
            rollback(connection, LOG);
            throw new DAOException(e);
        } finally {
            close(connection, LOG);
        }
    }
}
