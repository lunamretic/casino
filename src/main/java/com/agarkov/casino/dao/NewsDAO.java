package com.agarkov.casino.dao;

import com.agarkov.casino.entity.News;
import com.agarkov.casino.exception.DAOException;
import com.agarkov.casino.exception.PoolException;
import com.agarkov.casino.pool.ConnectionPool;
import com.agarkov.casino.pool.ProxyConnection;
import org.apache.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * The type News dao.
 */
public class NewsDAO extends AbstractDAO<News> {
    private static final Logger LOG = Logger.getLogger(NewsDAO.class);

    private static final int NEWS_PER_PAGE = 5;
    private static final String SQL_INSERT_ROW = "INSERT INTO news (headline, body, author_id, date) " +
            "VALUES (?, ?, ?, ?)";
    private static final String SQL_REMOVE_ROW = "DELETE FROM news WHERE id = ?";
    private static final String SQL_SELECT_NEWS = "SELECT news.id, headline, body, date, name FROM news, user " +
            "WHERE user.id = news.author_id ORDER BY news.id DESC LIMIT ?, ?";

    /**
     * Add news to database
     *
     * @param authorId the author id
     * @param news     the news
     * @throws DAOException the dao exception
     */
    public void add(long authorId, News news) throws DAOException {
        ProxyConnection connection;
        try {
            connection = ConnectionPool.getInstance().getConnection();
        } catch (PoolException e) {
            throw new DAOException(e);
        }

        try (PreparedStatement statement = connection.prepareStatement(SQL_INSERT_ROW)){
            connection.setAutoCommit(false);
            statement.setString(1, news.getHeadline());
            statement.setString(2, news.getBody());
            statement.setLong(3, authorId);
            statement.setObject(4, news.getDate());
            statement.executeUpdate();
            connection.commit();
        } catch (SQLException e) {
            rollback(connection, LOG);
            throw new DAOException(e);
        } finally {
            close(connection, LOG);
        }
    }

    /**
     * Remove news from database
     *
     * @param id the id
     * @throws DAOException the dao exception
     */
    public void remove(long id) throws DAOException {
        ProxyConnection connection;
        try {
            connection = ConnectionPool.getInstance().getConnection();
        } catch (PoolException e) {
            throw new DAOException(e);
        }
        try (PreparedStatement statement = connection.prepareStatement(SQL_REMOVE_ROW)) {
            connection.setAutoCommit(false);
            statement.setLong(1, id);
            statement.executeUpdate();
            connection.commit();
        } catch (SQLException e) {
            rollback(connection, LOG);
            throw new DAOException(e);
        } finally {
            close(connection, LOG);
        }
    }

    /**
     * Load page list.
     * Loads NEWS_PER_PAGE + 1 news to determine whether there is more news in the database.
     *
     * @param page the page
     * @return the list
     * @throws DAOException the dao exception
     */
    public List<News> loadPage(int page) throws DAOException {
        List<News> newsList = new ArrayList<>(NEWS_PER_PAGE + 1);
        try (ProxyConnection connection = ConnectionPool.getInstance().getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_SELECT_NEWS)) {
            statement.setInt(1, page * NEWS_PER_PAGE);
            statement.setInt(2, NEWS_PER_PAGE + 1);
            statement.executeQuery();

            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                News news = new News();
                news.setId(resultSet.getLong(1));
                news.setHeadline(resultSet.getString(2));
                news.setBody(resultSet.getString(3));
                news.setDate(resultSet.getTimestamp(4));
                news.setAuthor(resultSet.getString(5));

                newsList.add(news);
            }
        } catch (PoolException | SQLException e) {
            throw new DAOException(e);
        }

        return newsList;
    }
}