package com.agarkov.casino.dao;

import com.agarkov.casino.entity.Role;
import com.agarkov.casino.entity.User;
import com.agarkov.casino.exception.DAOException;
import com.agarkov.casino.exception.PoolException;
import com.agarkov.casino.pool.ConnectionPool;
import com.agarkov.casino.pool.ProxyConnection;
import org.apache.log4j.Logger;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * The type User dao.
 */
public class UserDAO extends AbstractDAO<User> {
    private static final Logger LOG = Logger.getLogger(UserDAO.class);

    private static final String SQL_INSERT_USER = "INSERT INTO user (username, password, email, name) " +
            "VALUES (?, ?, ?, ?)";
    private static final String SQL_CHECK_EXISTENCE = "SELECT 1 FROM user " +
            "WHERE username = ? OR password = ?";
    private static final String SQL_SELECT_BY_AUTHENTICATION = "SELECT * FROM user " +
            "WHERE username = ? AND password = ?";
    private static final String SQL_SELECT_CASH = "SELECT cash FROM user WHERE id = ?";
    private static final String SQL_UPDATE_EMAIL = "UPDATE user SET email = ? WHERE id = ?";
    private static final String SQL_UPDATE_NAME = "UPDATE user SET name = ? WHERE id = ?";
    private static final String SQL_UPDATE_PASSWORD = "UPDATE user SET password = ? WHERE id = ? AND password = ?";
    private static final String SQL_UPDATE_ROLE = "UPDATE user SET role = ? WHERE username = ?";
    private static final String SQL_UPDATE_AVATAR = "UPDATE user SET avatar = ? WHERE id = ?";
    private static final String SQL_DEPOSIT_CASH = "UPDATE user SET cash = cash + ? WHERE id = ?";
    private static final String SQL_WITHDRAW_CASH = "UPDATE user SET cash = cash - ? WHERE id = ?";

    /**
     * Update avatar boolean.
     *
     * @param id       the id
     * @param newValue the new value
     * @return the boolean
     * @throws DAOException the dao exception
     */
    public boolean updateAvatar(long id, String newValue) throws DAOException {
        return update(id, newValue, SQL_UPDATE_AVATAR);
    }

    /**
     * Update email boolean.
     *
     * @param id       the id
     * @param newValue the new value
     * @return the boolean
     * @throws DAOException the dao exception
     */
    public boolean updateEmail(long id, String newValue) throws DAOException {
        return update(id, newValue, SQL_UPDATE_EMAIL);
    }

    /**
     * Update name boolean.
     *
     * @param id       the id
     * @param newValue the new value
     * @return the boolean
     * @throws DAOException the dao exception
     */
    public boolean updateName(long id, String newValue) throws DAOException  {
        return update(id, newValue, SQL_UPDATE_NAME);
    }

    /**
     * Update password boolean.
     *
     * @param id              the id
     * @param currentPassword the current password
     * @param newPassword     the new password
     * @return the boolean
     * @throws DAOException the dao exception
     */
    public boolean updatePassword(long id, String currentPassword, String newPassword) throws DAOException {
        int count;
        ProxyConnection connection;
        try {
            connection = ConnectionPool.getInstance().getConnection();
        } catch (PoolException e) {
            throw new DAOException(e);
        }
        try (PreparedStatement statement = connection.prepareStatement(SQL_UPDATE_PASSWORD)) {
            connection.setAutoCommit(false);
            statement.setString(1, newPassword);
            statement.setLong(2, id);
            statement.setString(3, currentPassword);
            count = statement.executeUpdate();
            connection.commit();
        } catch (SQLException e) {
            rollback(connection, LOG);
            throw new DAOException(e);
        } finally {
            close(connection, LOG);
        }
        return count == 1;
    }

    /**
     * Update role boolean.
     *
     * @param newRole  the new role
     * @param username the username
     * @return the boolean
     * @throws DAOException the dao exception
     */
    public boolean updateRole(Role newRole, String username) throws DAOException {
        ProxyConnection connection;
        try {
            connection = ConnectionPool.getInstance().getConnection();
        } catch (PoolException e) {
            throw new DAOException(e);
        }
        try (PreparedStatement statement = connection.prepareStatement(SQL_UPDATE_ROLE)) {
            connection.setAutoCommit(false);
            statement.setString(1, newRole.toString());
            statement.setString(2, username);
            statement.executeUpdate();
            connection.commit();
        } catch (SQLException e) {
            rollback(connection, LOG);
            throw new DAOException(e);
        } finally {
            close(connection, LOG);
        }
        return true;
    }

    /**
     * Deposit cash boolean.
     *
     * @param id       the id
     * @param newValue the new value
     * @return the boolean
     * @throws DAOException the dao exception
     */
    public boolean depositCash(long id, String newValue) throws DAOException {
        return update(id, newValue, SQL_DEPOSIT_CASH);
    }

    /**
     * Withdraw cash boolean.
     *
     * @param id       the id
     * @param newValue the new value
     * @return the boolean
     * @throws DAOException the dao exception
     */
    public boolean withdrawCash(long id, String newValue) throws DAOException {
        return update(id, newValue, SQL_WITHDRAW_CASH);
    }

    /**
     * Check existence boolean.
     * Determine whether the username or email is already taken.
     *
     * @param username the username
     * @param email    the email
     * @return the boolean
     * @throws DAOException the dao exception
     */
    public boolean checkExistence(String username, String email) throws DAOException {
        boolean alreadyTaken = false;

        try (ProxyConnection connection = ConnectionPool.getInstance().getConnection();
        PreparedStatement statement = connection.prepareStatement(SQL_CHECK_EXISTENCE)) {
            statement.setString(1, username);
            statement.setString(2, email);
            statement.executeQuery();

            try (ResultSet resultSet = statement.executeQuery()) {
                if (resultSet.next()) {
                    alreadyTaken = true;
                }
            }
        } catch (PoolException | SQLException e) {
            throw new DAOException(e);
        }

        return alreadyTaken;
    }

    /**
     * Add new user to database.
     *
     * @param user     the user
     * @param password the password
     * @return the long
     * @throws DAOException the dao exception
     */
    public long add(User user, String password) throws DAOException {
        ProxyConnection connection;
        try {
            connection = ConnectionPool.getInstance().getConnection();
        } catch (PoolException e) {
            throw new DAOException(e);
        }

        long id;

        try (PreparedStatement statement = connection.prepareStatement(SQL_INSERT_USER,
                Statement.RETURN_GENERATED_KEYS)){
            connection.setAutoCommit(false);
            statement.setString(1, user.getUsername());
            statement.setString(2, password);
            statement.setString(3, user.getEmail());
            statement.setString(4, user.getUsername());
            statement.executeUpdate();
            connection.commit();
            try (ResultSet generatedKeys = statement.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    id = generatedKeys.getLong(1);
                }
                else {
                    throw new DAOException("Creating user failed, no ID obtained.");
                }
            }
        } catch (SQLException e) {
            rollback(connection, LOG);
            throw new DAOException(e);
        } finally {
            close(connection, LOG);
        }
        return id;
    }

    /**
     * Find user by username and password.
     *
     * @param username the username
     * @param password the password
     * @return the user
     * @throws DAOException the dao exception
     */
    public User findByAuthentication(String username, String password) throws DAOException {
        User user = null;

        try (ProxyConnection connection = ConnectionPool.getInstance().getConnection();
        PreparedStatement statement = connection.prepareStatement(SQL_SELECT_BY_AUTHENTICATION)) {
            statement.setString(1, username);
            statement.setString(2, password);
            statement.executeQuery();

            try (ResultSet resultSet = statement.executeQuery()) {
                if (resultSet.next()) {
                    user = new User();
                    user.setId(resultSet.getLong(1));
                    user.setUsername(resultSet.getString(2));
                    user.setEmail(resultSet.getString(3));
                    user.setName(resultSet.getString(5));
                    user.setRole(Role.valueOf(resultSet.getString(6).toUpperCase()));
                    user.setCash(resultSet.getInt(7));
                    user.setImageSrc(resultSet.getString(8));
                }
            }
        } catch (PoolException | SQLException e) {
            throw new DAOException(e);
        }

        return user;
    }

    /**
     * Find cash int.
     *
     * @param id the id
     * @return the int
     * @throws DAOException the dao exception
     */
    public int findCash(long id) throws DAOException {
        int cash = 0;

        try (ProxyConnection connection = ConnectionPool.getInstance().getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_SELECT_CASH)) {
            statement.setLong(1, id);
            statement.executeQuery();

            try (ResultSet resultSet = statement.executeQuery()) {
                if (resultSet.next()) {
                    cash = resultSet.getInt(1);
                }
            }
        } catch (PoolException | SQLException e) {
            throw new DAOException(e);
        }

        return cash;
    }

    private boolean update(long id, String newValue, String sql) throws DAOException {
        ProxyConnection connection;
        try {
            connection = ConnectionPool.getInstance().getConnection();
        } catch (PoolException e) {
            throw new DAOException(e);
        }
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            connection.setAutoCommit(false);
            statement.setString(1, newValue);
            statement.setLong(2, id);
            statement.executeUpdate();
            connection.commit();
        } catch (SQLException e) {
            rollback(connection, LOG);
            throw new DAOException(e);
        } finally {
            close(connection, LOG);
        }
        return true;
    }
}