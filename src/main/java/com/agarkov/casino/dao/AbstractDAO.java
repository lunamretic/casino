package com.agarkov.casino.dao;

import com.agarkov.casino.entity.Entity;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * The type Abstract dao.
 *
 * @param <T> the type parameter
 */
public abstract class AbstractDAO<T extends Entity> {
    private static final String ERROR_AUTO_COMMIT = "Can't setAutoCommit true and close connection";
    private static final String ERROR_ROLLBACK = "Can't rollback";

    /**
     * Close connection
     *
     * @param connection the connection
     * @param log        the log
     */
    protected void close(Connection connection, Logger log) {
        try {
            connection.setAutoCommit(true);
            connection.close();
        } catch (SQLException e) {
            log.error(ERROR_AUTO_COMMIT, e);
        }
    }

    /**
     * Rollback changes
     *
     * @param connection the connection
     * @param log        the log
     */
    protected void rollback(Connection connection, Logger log) {
        try {
            connection.rollback();
        } catch (SQLException ex) {
            log.error(ERROR_ROLLBACK, ex);
        }
    }
}