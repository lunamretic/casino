package com.agarkov.casino.dao;

import com.agarkov.casino.entity.InformationCard;
import com.agarkov.casino.entity.Role;
import com.agarkov.casino.exception.DAOException;
import com.agarkov.casino.exception.PoolException;
import com.agarkov.casino.pool.ConnectionPool;
import com.agarkov.casino.pool.ProxyConnection;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * The type Information card dao.
 */
public class InformationCardDAO extends AbstractDAO {
    private static final String SQL_SELECT_PLAYER = "SELECT username, email, name, role, avatar, " +
            "(SELECT COUNT(winner) " +
            "FROM game WHERE (player1 = user.id AND winner < 0) OR (player2 = user.id AND winner > 0)) AS win, " +
            "(SELECT COUNT(winner) FROM game " +
            "WHERE (player1 = user.id AND winner > 0) OR (player2 = user.id AND winner < 0)) AS lose, " +
            "(SELECT COUNT(winner) FROM game " +
            "WHERE ((player1 = user.id OR player2 = user.id) AND winner = 0)) AS draw, " +
            "(SELECT win / (win + lose) * 100) AS win_rate " +
            "FROM user WHERE username = ?";

    private static final String SQL_SELECT_TOP = "SELECT username, email, name, " +
            "(SELECT COUNT(winner) FROM game " +
            "WHERE (player1 = user.id AND winner < 0) OR (player2 = user.id AND winner > 0)) AS win," +
            "(SELECT COUNT(winner) FROM game " +
            "WHERE (player1 = user.id AND winner > 0) OR (player2 = user.id AND winner < 0)) AS lose, " +
            "(SELECT COUNT(winner) FROM game " +
            "WHERE ((player1 = user.id OR player2 = user.id) AND winner = 0)) AS draw, " +
            "(SELECT win / (win + lose) * 100) AS win_rate " +
            "FROM user WHERE (SELECT 1 FROM game WHERE player1 = user.id || player2 = user.id LIMIT 1) IS NOT NULL " +
            "ORDER BY win_rate DESC LIMIT 10";

    /**
     * Find player information card.
     *
     * @param username the username
     * @return the information card
     * @throws DAOException the dao exception
     */
    public InformationCard findPlayer(String username) throws DAOException {
        InformationCard informationCard = null;
        try (ProxyConnection connection = ConnectionPool.getInstance().getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_SELECT_PLAYER)) {
            statement.setString(1, username);
            statement.executeQuery();

            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                informationCard = new InformationCard();
                informationCard.setUsername(resultSet.getString(1));
                informationCard.setEmail(resultSet.getString(2));
                informationCard.setName(resultSet.getString(3));
                informationCard.setRole(Role.valueOf(resultSet.getString(4).toUpperCase()));
                informationCard.setImageSrc(resultSet.getString(5));
                informationCard.setWin(resultSet.getInt(6));
                informationCard.setLose(resultSet.getInt(7));
                informationCard.setDraw(resultSet.getInt(8));
                informationCard.setWinRate(resultSet.getDouble(9));
            }
        } catch (PoolException | SQLException e) {
            throw new DAOException(e);
        }

        return informationCard;
    }

    /**
     * Find top players list.
     *
     * @return the list
     * @throws DAOException the dao exception
     */
    public List<InformationCard> findTopPlayers() throws DAOException {
        List<InformationCard> topPlayers = new ArrayList<>(10);
        try (ProxyConnection connection = ConnectionPool.getInstance().getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_SELECT_TOP)) {
            statement.executeQuery();

            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                InformationCard informationCard = new InformationCard();
                informationCard.setNumber(resultSet.getRow());
                informationCard.setUsername(resultSet.getString(1));
                informationCard.setEmail(resultSet.getString(2));
                informationCard.setName(resultSet.getString(3));
                informationCard.setWin(resultSet.getInt(4));
                informationCard.setLose(resultSet.getInt(5));
                informationCard.setDraw(resultSet.getInt(6));
                informationCard.setWinRate(resultSet.getDouble(7));

                topPlayers.add(informationCard);
            }
        } catch (PoolException | SQLException e) {
            throw new DAOException(e);
        }

        return topPlayers;
    }
}
