package com.agarkov.casino.logic;

import com.agarkov.casino.entity.game.Card;
import com.agarkov.casino.entity.game.Rank;
import com.agarkov.casino.entity.game.Suit;

import java.util.Collections;
import java.util.LinkedList;
import java.util.Random;

/**
 * The type Deck handler.
 */
public class DeckHandler {
    /** Do not instantiate DeckHandler. */
    private DeckHandler() {
        throw new IllegalAccessError("Utility class");
    }

    /**
     * Create deck linked list.
     *
     * @return the linked list
     */
    public static LinkedList<Card> createDeck() {
        LinkedList<Card> deck = new LinkedList<>();

        for (Suit suit : Suit.values()) {
            for (Rank rank : Rank.values()) {
                Card card = new Card();
                card.setSuit(suit);
                card.setRank(rank);
                card.setValue(rank.getValue());
                card.setImgSrc("/img/game/card_asset/" + suit.toString().toLowerCase() + "_" + rank.toString().toLowerCase() + ".png");
                deck.add(card);
            }
        }

        long seed = System.nanoTime();
        Collections.shuffle(deck, new Random(seed));

        return deck;
    }
}
