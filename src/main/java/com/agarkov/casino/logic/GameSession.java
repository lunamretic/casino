package com.agarkov.casino.logic;

import com.agarkov.casino.entity.Entity;
import com.agarkov.casino.entity.game.Card;
import com.agarkov.casino.entity.game.Deck;
import com.agarkov.casino.entity.game.Player;
import com.agarkov.casino.entity.game.response.*;
import com.agarkov.casino.exception.GameSessionException;

import java.util.ArrayList;
import java.util.concurrent.ArrayBlockingQueue;

/**
 * The type Game session.
 */
public class GameSession implements Entity {
    private final long id;
    private final Player player1;
    private final Player player2;
    private final boolean isAI;

    private Deck deck;
    private int winner;
    private int bet;
    private int currentPlayer;
    private boolean isFinished;
    private boolean isSaved;
    //Stores player's actions for polling commands
    private ArrayBlockingQueue<String> player1Actions;
    private ArrayBlockingQueue<String> player2Actions;

    /**
     * Instantiates a new Game session.
     *
     * @param id      the id
     * @param player1 the player 1
     * @param player2 the player 2
     * @param isAI    the is ai
     */
    public GameSession(long id, Player player1, Player player2, boolean isAI) {
        deck = new Deck(DeckHandler.createDeck());
        player1Actions = new ArrayBlockingQueue<>(5, true);
        player2Actions = new ArrayBlockingQueue<>(5, true);
        this.id = id;
        this.player1 = player1;
        this.player2 = player2;
        this.isAI = isAI;
        currentPlayer = 1;
    }

    /**
     * Gets id.
     *
     * @return the id
     */
    public long getId() {
        return id;
    }

    /**
     * Gets player 1.
     *
     * @return the player 1
     */
    public Player getPlayer1() {
        return player1;
    }

    /**
     * Gets player 2.
     *
     * @return the player 2
     */
    public Player getPlayer2() {
        return player2;
    }

    /**
     * Is ai boolean.
     *
     * @return the boolean
     */
    public boolean isAI() {
        return isAI;
    }

    /**
     * Gets deck.
     *
     * @return the deck
     */
    public Deck getDeck() {
        return deck;
    }

    /**
     * Sets deck.
     *
     * @param deck the deck
     */
    public void setDeck(Deck deck) {
        this.deck = deck;
    }

    /**
     * Gets winner.
     *
     * @return the winner
     */
    public int getWinner() {
        return winner;
    }

    /**
     * Sets winner.
     *
     * @param winner the winner
     */
    public void setWinner(int winner) {
        this.winner = winner;
    }

    /**
     * Gets bet.
     *
     * @return the bet
     */
    public int getBet() {
        return bet;
    }

    /**
     * Sets bet.
     *
     * @param bet the bet
     */
    public void setBet(int bet) {
        this.bet = bet;
    }

    /**
     * Gets current player.
     *
     * @return the current player
     */
    public int getCurrentPlayer() {
        return currentPlayer;
    }

    /**
     * Sets current player.
     *
     * @param currentPlayer the current player
     */
    public void setCurrentPlayer(int currentPlayer) {
        this.currentPlayer = currentPlayer;
    }

    /**
     * Is finished boolean.
     *
     * @return the boolean
     */
    public boolean isFinished() {
        return isFinished;
    }

    /**
     * Sets finished.
     *
     * @param finished the finished
     */
    public void setFinished(boolean finished) {
        isFinished = finished;
    }

    /**
     * Is saved boolean.
     *
     * @return the boolean
     */
    public boolean isSaved() {
        return isSaved;
    }

    /**
     * Sets saved.
     *
     * @param saved the saved
     */
    public void setSaved(boolean saved) {
        isSaved = saved;
    }

    /**
     * Gets player 1 actions.
     *
     * @return the player 1 actions
     */
    public ArrayBlockingQueue<String> getPlayer1Actions() {
        return player1Actions;
    }

    /**
     * Sets player 1 actions.
     *
     * @param player1Actions the player 1 actions
     */
    public void setPlayer1Actions(ArrayBlockingQueue<String> player1Actions) {
        this.player1Actions = player1Actions;
    }

    /**
     * Gets player 2 actions.
     *
     * @return the player 2 actions
     */
    public ArrayBlockingQueue<String> getPlayer2Actions() {
        return player2Actions;
    }

    /**
     * Sets player 2 actions.
     *
     * @param player2Actions the player 2 actions
     */
    public void setPlayer2Actions(ArrayBlockingQueue<String> player2Actions) {
        this.player2Actions = player2Actions;
    }

    /**
     * Gets player points.
     *
     * @param playerId the player id
     * @return the player points
     * @throws GameSessionException the game session exception
     */
    public int getPlayerPoints(long playerId) throws GameSessionException {
        Player player = definePlayer(playerId);
        return player.getPoints();
    }

    /**
     * Gets player cash.
     *
     * @param playerId the player id
     * @return the player cash
     * @throws GameSessionException the game session exception
     */
    public int getPlayerCash(long playerId) throws GameSessionException {
        Player player = definePlayer(playerId);
        return player.getCash();
    }

    /**
     * Gets opponent points.
     *
     * @param playerId the player id
     * @return the opponent points
     * @throws GameSessionException the game session exception
     */
    public int getOpponentPoints(long playerId) throws GameSessionException {
        Player opponent = defineOpponent(playerId);
        return opponent.getPoints();
    }

    /**
     * Gets opponent hand.
     *
     * @param playerId the player id
     * @return the opponent hand
     * @throws GameSessionException the game session exception
     */
    public ArrayList<Card> getOpponentHand(long playerId) throws GameSessionException {
        Player opponent = defineOpponent(playerId);
        return opponent.getHand();
    }

    /**
     * Gets opponent name.
     *
     * @param playerId the player id
     * @return the opponent name
     * @throws GameSessionException the game session exception
     */
    public String getOpponentName(long playerId) throws GameSessionException {
        Player opponent = defineOpponent(playerId);
        return opponent.getUsername();
    }

    /**
     * Define turn boolean.
     *
     * @param playerId the player id
     * @return the boolean
     * @throws GameSessionException the game session exception
     */
    public boolean defineTurn(long playerId) throws GameSessionException {
        Player player = definePlayer(playerId);
        return player.getNumber() == currentPlayer;
    }

    /**
     * Place bet.
     *
     * @param playerId the player id
     * @param bet      the bet
     * @throws GameSessionException the game session exception
     */
    public void placeBet(long playerId, int bet) throws GameSessionException {
        Player player = definePlayer(playerId);
        if (!validateTurn(player)) {
            throw new GameSessionException("Wrong turn!");
        }
        if (player.getCash() <= bet) {
            throw new GameSessionException("Not enough cash!");
        }
        this.bet = bet;
        if (!isAI) {
            player1Actions.add("offered-bet");
        }
    }

    /**
     * Accept bet.
     *
     * @param playerId the player id
     * @throws GameSessionException the game session exception
     */
    public void acceptBet(long playerId) throws GameSessionException {
        Player player = definePlayer(playerId);
        if (validateTurn(player)) {
            throw new GameSessionException("Wrong player turn!");
        }
        player2Actions.add("bet-accepted");
    }

    /**
     * Refuse bet.
     *
     * @param playerId the player id
     * @throws GameSessionException the game session exception
     */
    public void refuseBet(long playerId) throws GameSessionException {
        Player player = definePlayer(playerId);
        if (validateTurn(player)) {
            throw new GameSessionException("Wrong player turn!");
        }
        player2Actions.add("bet-refused");
        updateCash();
    }

    /**
     * Polling server response.
     *
     * @param playerId the player id
     * @return the server response
     * @throws GameSessionException the game session exception
     */
    public ServerResponse polling(long playerId) throws GameSessionException {
        Player player = definePlayer(playerId);
        String type;
        if (player == player1) {
            type = player2Actions.poll();
        } else {
            type = player1Actions.poll();
        }

        if (type != null) {
            switch (type) {
                case "offered-bet":
                    return new OfferedBetResponse(type, bet);
                case "bet-accepted":
                case "card":
                case "hand":
                case "new-game":
                    return new PollingResponse(type);
                case "stand":
                    currentPlayer = 2;
                    return new PollingResponse(type);
                case "results":
                    StandResponse serverResponse = new StandResponse();
                    String result = determineResults(playerId);
                    int opponentPoint = getOpponentPoints(playerId);
                    int cash = player1.getCash();
                    ArrayList<Card> opponentHand = getOpponentHand(playerId);

                    serverResponse.setType(type);
                    serverResponse.setMessage(result);
                    serverResponse.setOpponentPoints(opponentPoint);
                    serverResponse.setOpponentHand(opponentHand);
                    serverResponse.setCash(cash);
                    return serverResponse;
                case "bet-refused":
                case "game-finished":
                case "user-left":
                    return new FinishGameResponse(type);
                default:
                    throw new GameSessionException("Wrong action!");
            }
        }
        return null;
    }

    /**
     * Receive start hand array list.
     *
     * @param playerId the player id
     * @return the array list
     * @throws GameSessionException the game session exception
     */
    public ArrayList<Card> receiveStartHand(long playerId) throws GameSessionException {
        Player player = definePlayer(playerId);
        if (!validateTurn(player)) {
            throw new GameSessionException("Wrong player turn!");
        }
        player.takeCard(deck.drawCard());
        player.takeCard(deck.drawCard());
        if (!isAI) {
            if (currentPlayer == 1) {
                player1Actions.add("hand");
            } else {
                player2Actions.add("hand");
            }
        }
        return player.getHand();
    }

    /**
     * Draw card card.
     *
     * @param playerId the player id
     * @return the card
     * @throws GameSessionException the game session exception
     */
    public Card drawCard(long playerId) throws GameSessionException {
        Player player = definePlayer(playerId);
        if (!validateTurn(player)) {
            throw new GameSessionException("Wrong player turn!");
        }
        if (player.getPoints() >= 21) {
            throw new GameSessionException("Can't draw cards with 21+ points!");
        }
        Card card = deck.drawCard();
        player.takeCard(card);
        if (!isAI) {
            if (currentPlayer == 1) {
                player1Actions.add("card");
            } else {
                player2Actions.add("card");
            }
        }
        return card;
    }

    /**
     * Stand boolean.
     *
     * @param playerId the player id
     * @return the boolean
     * @throws GameSessionException the game session exception
     */
    public boolean stand(long playerId) throws GameSessionException {
        Player player = definePlayer(playerId);
        if (!validateTurn(player)) {
            throw new GameSessionException("Wrong player turn!");
        }
        boolean isSecondPlayerStand = false;
        if (currentPlayer == 1) {
            player1Actions.add("stand");
        } else {
            isSecondPlayerStand = true;
            player2Actions.add("results");
            isFinished = true;
        }

        return isSecondPlayerStand;
    }

    /**
     * Update cash.
     */
    public void updateCash() {
        int player1Cash = player1.getCash();
        int player2Cash = player2.getCash();

        if (winner < 0) {
            player1.setCash(player1Cash + bet);
            player2.setCash(player2Cash - bet);
        } else {
            if (winner > 0) {
                player1.setCash(player1Cash - bet);
                player2.setCash(player2Cash + bet);
            }
        }
    }

    /**
     * Determine results string.
     *
     * @param playerId the player id
     * @return the string
     * @throws GameSessionException the game session exception
     */
    public String determineResults(long playerId) throws GameSessionException {
        Player player = definePlayer(playerId);
        String result;
        winner = determineWinner(player1, player2);
        if (winner == 0) {
            result = "draw";
        } else {
            if (winner < 0 && player == player1) {
                result = "win";
            } else {
                if (winner > 0 && player == player2) {
                    result = "win";
                } else {
                    result = "lose";
                }
            }
        }
        return result;
    }


    /**
     * Stand ai string.
     *
     * @param playerId the player id
     * @return the string
     * @throws GameSessionException the game session exception
     */
    public String standAI(long playerId) throws GameSessionException {
        turnAI(player2);
        return determineResults(playerId);
    }

    /**
     * Offer new game.
     *
     * @param playerId the player id
     * @throws GameSessionException the game session exception
     */
    public void offerNewGame(long playerId) throws GameSessionException {
        Player player = definePlayer(playerId);
        if (player == player1) {
            if ("offered-new-game".equals(player2Actions.peek())) {
                player2Actions.remove("offered-new-game");
                newGame();
            } else {
                player1Actions.add("offered-new-game");
            }
        } else {
            if ("offered-new-game".equals(player1Actions.peek())) {
                player1Actions.remove("offered-new-game");
                newGame();
            } else {
                player2Actions.add("offered-new-game");
            }
        }
    }


    /**
     * Leave game string.
     *
     * @param playerId the player id
     * @return the string
     * @throws GameSessionException the game session exception
     */
    public String leaveGame(long playerId) throws GameSessionException {
        Player player = definePlayer(playerId);
        String leaveCondition;
        if (isFinished) {
            leaveCondition = "game-finished";
        } else {
            if (bet == 0) {
                leaveCondition = "bet-refused";
            } else {
                winner = player == player1 ? 1 : -1;
                leaveCondition = "user-left";
            }
        }
        return leaveCondition;
    }

    /**
     * Inform leave.
     *
     * @param playerId       the player id
     * @param leaveCondition the leave condition
     * @throws GameSessionException the game session exception
     */
    public void informLeave(long playerId, String leaveCondition) throws GameSessionException {
        Player player = definePlayer(playerId);
        if (player == player1) {
            player1Actions.add(leaveCondition);
        } else {
            player2Actions.add(leaveCondition);
        }
    }

    /**
     * New game.
     */
    public void newGame() {
        isFinished = false;
        isSaved = false;
        currentPlayer = 1;
        deck = new Deck(DeckHandler.createDeck());
        player1.clearHand();
        player2.clearHand();
        player1Actions.clear();
        player2Actions.clear();
        player1Actions.add("new-game");
        player2Actions.add("new-game");
    }

    private Player definePlayer(long id) throws GameSessionException {
        Player player = null;
        if (player1.getId() == id) {
            player = player1;
        }
        if (!isAI && player2.getId() == id) {
            player = player2;
        }
        if (player == null) {
            throw new GameSessionException("Invalid user (" + id + ") in game session!");
        }
        return player;
    }

    private Player defineOpponent(long id) throws GameSessionException {
        Player player = definePlayer(id);
        if (player == player1) {
            return player2;
        } else {
            return player1;
        }
    }

    private int determineWinner(Player player1, Player player2) {
        if (player1.getPoints() > 21 && player2.getPoints() > 21) {
            return 0;
        }
        if (player1.getPoints() <= 21 && player2.getPoints() > 21) {
            return -1;
        }
        if (player1.getPoints() > 21 && player2.getPoints() <= 21) {
            return 1;
        }
        return player2.getPoints() - player1.getPoints();
    }

    private void turnAI(Player playerAI) {
        playerAI.takeCard(deck.drawCard());
        playerAI.takeCard(deck.drawCard());
        while (playerAI.getPoints() < 17) {
            playerAI.takeCard(deck.drawCard());
        }
    }

    private boolean validateTurn(Player player) {
        return player.getNumber() == currentPlayer;
    }
}
