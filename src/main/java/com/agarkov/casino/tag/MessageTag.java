package com.agarkov.casino.tag;

import com.agarkov.casino.manager.MessageManager;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.BodyTagSupport;
import java.io.IOException;

/**
 * Custom tag, displays message key according to user's locale
 */
public class MessageTag extends BodyTagSupport {
    private static final String ATTR_LOCALE = "locale";
    private static final String RU = "ru_RU";

    private String messageKey;

    /**
     * Sets message key.
     *
     * @param messageKey the message key
     */
    public void setMessageKey(String messageKey) {
        this.messageKey = messageKey;
    }

    @Override
    public int doStartTag() throws JspException {
        if (!messageKey.isEmpty()) {
            MessageManager messageManager;
            if (RU.equals(pageContext.getSession().getAttribute(ATTR_LOCALE))) {
                messageManager = MessageManager.RU;
            } else {
                messageManager = MessageManager.EN;
            }

            try {
                pageContext.getOut().print(messageManager.getMessage(messageKey));
            } catch (IOException e) {
                throw new JspException(e);
            }
        }

        return SKIP_BODY;
    }
}