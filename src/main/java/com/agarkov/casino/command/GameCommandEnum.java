package com.agarkov.casino.command;

import com.agarkov.casino.command.game.*;
import com.agarkov.casino.command.game.preparation.CancelSearchCommand;
import com.agarkov.casino.command.game.preparation.SearchOpponentCommand;
import com.agarkov.casino.command.game.preparation.WaitPlayerCommand;

/**
 * The enum Game command enum.
 */
public enum GameCommandEnum {
    /**
     * The Game define turn.
     */
    GAME_DEFINE_TURN {
        {
            command = new DefineTurnCommand();
        }
    },
    /**
     * The Game polling.
     */
    GAME_POLLING {
        {
            command = new PollingCommand();
        }
    },
    /**
     * The Game place bet.
     */
    GAME_PLACE_BET {
        {
            command = new PlaceBetCommand();
        }
    },
    /**
     * The Game accept bet.
     */
    GAME_ACCEPT_BET {
        {
            command = new AcceptBetCommand();
        }
    },
    /**
     * The Game refuse bet.
     */
    GAME_REFUSE_BET {
        {
            command = new RefuseBetCommand();
        }
    },
    /**
     * The Game start hand.
     */
    GAME_START_HAND {
        {
            command = new StartHandCommand();
        }
    },
    /**
     * The Game draw card.
     */
    GAME_DRAW_CARD {
        {
            command = new DrawCardCommand();
        }
    },
    /**
     * The Game stand.
     */
    GAME_STAND {
        {
            command = new StandCommand();
        }
    },
    /**
     * The Game stand ai.
     */
    GAME_STAND_AI {
        {
            command = new StandAICommand();
        }
    },
    /**
     * The Search opponent.
     */
    SEARCH_OPPONENT{
        {
            command = new SearchOpponentCommand();
        }
    },
    /**
     * The Wait player.
     */
    WAIT_PLAYER {
        {
            command = new WaitPlayerCommand();
        }
    },
    /**
     * The Cancel search.
     */
    CANCEL_SEARCH {
        {
            command = new CancelSearchCommand();
        }
    },
    /**
     * The Game offer new game.
     */
    GAME_OFFER_NEW_GAME {
        {
            command = new OfferNewGameCommand();
        }
    },
    /**
     * The Game leave.
     */
    GAME_LEAVE {
        {
            command = new LeaveGameCommand();
        }
    };

    /**
     * The Command.
     */
    GameCommand command;

    /**
     * Gets current command.
     *
     * @return the current command
     */
    public GameCommand getCurrentCommand() {
        return  command;
    }
}
