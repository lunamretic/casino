package com.agarkov.casino.command;

import com.agarkov.casino.command.additional.LanguageCommand;
import com.agarkov.casino.command.additional.PageCommand;
import com.agarkov.casino.command.additional.RatingCommand;
import com.agarkov.casino.command.authorization.LogInCommand;
import com.agarkov.casino.command.authorization.LogOutCommand;
import com.agarkov.casino.command.authorization.RegisterCommand;
import com.agarkov.casino.command.game.preparation.StartGameAICommand;
import com.agarkov.casino.command.game.preparation.StartGameCommand;
import com.agarkov.casino.command.news.AddNewsCommand;
import com.agarkov.casino.command.news.RemoveNewsCommand;
import com.agarkov.casino.command.news.NewsCommand;
import com.agarkov.casino.command.user.*;

/**
 * The enum Command enum.
 */
public enum CommandEnum {
    /**
     * The Page.
     */
    PAGE {
        {
            command = new PageCommand();
        }
    },
    /**
     * The News.
     */
    NEWS {
        {
            command = new NewsCommand();
        }
    },
    /**
     * The News add.
     */
    NEWS_ADD {
        {
            command = new AddNewsCommand();
        }
    },
    /**
     * The News remove.
     */
    NEWS_REMOVE {
        {
            command = new RemoveNewsCommand();
        }
    },
    /**
     * The Rating.
     */
    RATING {
        {
            command = new RatingCommand();
        }
    },
    /**
     * The Register.
     */
    REGISTER {
        {
            command = new RegisterCommand();
        }
    },
    /**
     * The Log in.
     */
    LOG_IN {
        {
            command = new LogInCommand();
        }
    },
    /**
     * The Log out.
     */
    LOG_OUT {
        {
            command = new LogOutCommand();
        }
    },
    /**
     * The Locale.
     */
    LOCALE {
        {
            command = new LanguageCommand();
        }
    },
    /**
     * The Profile.
     */
    PROFILE {
        {
            command = new ProfileCommand();
        }
    },
    /**
     * The Payment.
     */
    PAYMENT {
        {
            command = new PaymentCommand();
        }
    },
    /**
     * The Update email.
     */
    UPDATE_EMAIL {
        {
            command = new UpdateEmailCommand();
        }
    },
    /**
     * The Update name.
     */
    UPDATE_NAME {
        {
            command = new UpdateNameCommand();
        }
    },
    /**
     * The Update password.
     */
    UPDATE_PASSWORD {
        {
            command = new UpdatePasswordCommand();
        }
    },
    /**
     * The Update role.
     */
    UPDATE_ROLE {
        {
            command = new UpdateRoleCommand();
        }
    },
    /**
     * The Start game ai.
     */
    START_GAME_AI {
        {
            command = new StartGameAICommand();
        }
    },
    /**
     * The Start game.
     */
    START_GAME {
        {
            command = new StartGameCommand();
        }
    };

    ActionCommand command;

    /**
     * Gets current command.
     *
     * @return the current command
     */
    public ActionCommand getCurrentCommand() {
        return  command;
    }
}
