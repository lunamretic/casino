package com.agarkov.casino.command;

import com.agarkov.casino.entity.game.response.ServerResponse;
import com.agarkov.casino.exception.CommandException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * The interface Game command.
 */
public interface GameCommand {
    /**
     * Execute server response.
     *
     * @param request  the request
     * @param response the response
     * @return the server response
     * @throws CommandException the command exception
     */
    ServerResponse execute(HttpServletRequest request, HttpServletResponse response) throws CommandException;
}
