package com.agarkov.casino.command.additional;

import com.agarkov.casino.command.ActionCommand;
import com.agarkov.casino.exception.CommandException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * The command changes the website locale for user.
 */
public class LanguageCommand implements ActionCommand {
    private static final String ATTR_LOCALE = "locale";
    private static final String ATTR_SELECT_EN = "selectEn";
    private static final String ATTR_SELECT_RU = "selectRu";
    private static final String REFERER = "referer";
    private static final String RU = "ru_RU";
    private static final String SELECTED = "selected";

    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        String url = request.getHeader(REFERER);
        String paramStr = url.substring(url.indexOf('?') + 1, url.length());
        HttpSession session = request.getSession();
        session.setAttribute(ATTR_LOCALE, request.getParameter(ATTR_LOCALE));

        if ((RU).equals(session.getAttribute(ATTR_LOCALE))) {
            session.setAttribute(ATTR_SELECT_RU, SELECTED);
            session.setAttribute(ATTR_SELECT_EN, " ");
        } else {
            session.setAttribute(ATTR_SELECT_RU, " ");
            session.setAttribute(ATTR_SELECT_EN, SELECTED);
        }

        return paramStr;
    }
}
