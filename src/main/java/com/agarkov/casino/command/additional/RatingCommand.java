package com.agarkov.casino.command.additional;

import com.agarkov.casino.command.ActionCommand;
import com.agarkov.casino.entity.InformationCard;
import com.agarkov.casino.exception.CommandException;
import com.agarkov.casino.exception.ServiceException;
import com.agarkov.casino.manager.PageManager;
import com.agarkov.casino.service.InformationCardService;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * The command sets the list of top players for rating page.
 */
public class RatingCommand implements ActionCommand {
    private static final String ATTR_RATING = "topPlayers";

    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        InformationCardService informationCardService = InformationCardService.getInstance();

        List<InformationCard> topPlayers;

        try {
            topPlayers = informationCardService.findTopPlayers();
        } catch (ServiceException e) {
            throw new CommandException(e);
        }

        request.setAttribute(ATTR_RATING, topPlayers);

        return PageManager.getPage("rating");
    }
}
