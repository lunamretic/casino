package com.agarkov.casino.command.additional;

import com.agarkov.casino.command.ActionCommand;
import com.agarkov.casino.exception.CommandException;
import com.agarkov.casino.manager.PageManager;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * The command defines the page to witch the user will be forwarded.
 */
public class PageCommand implements ActionCommand {
    private static final String ATTR_MESSAGE = "message";
    private static final String PARAM_PAGE = "page";

    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        HttpSession session = request.getSession();
        if (session.getAttribute(ATTR_MESSAGE) != null) {
            String message = session.getAttribute(ATTR_MESSAGE).toString();
            session.removeAttribute(ATTR_MESSAGE);
            request.setAttribute(ATTR_MESSAGE, message);
        }
        return PageManager.getPage(request.getParameter(PARAM_PAGE));
    }
}