package com.agarkov.casino.command;

import com.agarkov.casino.exception.CommandException;

import javax.servlet.http.HttpServletRequest;

/**
 * The interface Action command.
 */
public interface ActionCommand {
    /**
     * Execute string.
     *
     * @param request the HttpServletRequest
     * @return the path to page or url query
     * @throws CommandException the command exception
     */
    String execute(HttpServletRequest request) throws CommandException;
}
