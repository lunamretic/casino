package com.agarkov.casino.command;

import javax.servlet.http.HttpServletRequest;

/**
 * The type Action factory.
 */
public class ActionFactory {
    private static final String COMMAND_PARAM = "command";

    /** Do not instantiate ActionFactory. */
    private ActionFactory() {
        throw new IllegalAccessError("Utility class");
    }

    /**
     * Defines the command for execution.
     *
     * @param request the HttpServletRequest
     * @return the action command
     */
    public static ActionCommand defineCommand(HttpServletRequest request) {
        ActionCommand currentCommand = new EmptyCommand();
        String action = request.getParameter(COMMAND_PARAM);
        if (action == null || action.isEmpty()) {
            return currentCommand;
        }
        CommandEnum currentEnum = CommandEnum.valueOf(action.toUpperCase());
        currentCommand = currentEnum.getCurrentCommand();
        return currentCommand;
    }
}
