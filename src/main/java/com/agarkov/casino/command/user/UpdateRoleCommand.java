package com.agarkov.casino.command.user;

import com.agarkov.casino.command.ActionCommand;
import com.agarkov.casino.entity.Role;
import com.agarkov.casino.exception.CommandException;
import com.agarkov.casino.exception.ServiceException;
import com.agarkov.casino.service.UserService;

import javax.servlet.http.HttpServletRequest;

/**
 * The command updates the user's role.
 */
public class UpdateRoleCommand implements ActionCommand {
    private static final String PARAM_ROLE = "role";
    private static final String PARAM_USERNAME = "username";

    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        UserService userService = UserService.getInstance();

        Role role = Role.valueOf(request.getParameter(PARAM_ROLE).toUpperCase());
        String username = request.getParameter(PARAM_USERNAME);

        try {
            userService.updateRole(role, username);
        } catch (ServiceException e) {
            throw new CommandException(e);
        }

        return "command=profile&username=".concat(username);
    }
}