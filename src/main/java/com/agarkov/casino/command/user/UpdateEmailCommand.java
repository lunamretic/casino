package com.agarkov.casino.command.user;

import com.agarkov.casino.command.ActionCommand;
import com.agarkov.casino.entity.User;
import com.agarkov.casino.exception.CommandException;
import com.agarkov.casino.exception.ServiceException;
import com.agarkov.casino.manager.PageManager;
import com.agarkov.casino.service.UserService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.MissingResourceException;

/**
 * The command updates the user's email.
 */
public class UpdateEmailCommand implements ActionCommand {
    private static final String ATTR_ERROR_MESSAGE = "errorMessage";
    private static final String ATTR_MESSAGE = "message";
    private static final String ATTR_USER = "user";
    private static final String PARAM_EMAIL = "email";

    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        HttpSession session = request.getSession();
        UserService userService = UserService.getInstance();
        User user = (User)session.getAttribute(ATTR_USER);
        long id = user.getId();
        String email = request.getParameter(PARAM_EMAIL);
        String page;

        try {
            String errorMessage = userService.updateEmail(id, email);

            if (errorMessage == null) {
                user.setEmail(email);
                session.setAttribute(ATTR_USER, user);
                session.setAttribute(ATTR_MESSAGE, "message.saved");
                page = "command=page&page=settings";
            } else {
                request.setAttribute(ATTR_ERROR_MESSAGE, errorMessage);
                page = PageManager.getPage("settings");
            }
        } catch (MissingResourceException | ServiceException e) {
            throw new CommandException(e);
        }

        return page;
    }
}
