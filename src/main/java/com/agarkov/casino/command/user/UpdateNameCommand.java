package com.agarkov.casino.command.user;

import com.agarkov.casino.command.ActionCommand;
import com.agarkov.casino.entity.User;
import com.agarkov.casino.exception.CommandException;
import com.agarkov.casino.exception.ServiceException;
import com.agarkov.casino.manager.PageManager;
import com.agarkov.casino.service.UserService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.MissingResourceException;

/**
 * The command updates the user's name.
 */
public class UpdateNameCommand implements ActionCommand {
    private static final String ATTR_ERROR_MESSAGE = "errorMessage";
    private static final String ATTR_MESSAGE = "message";
    private static final String ATTR_USER = "user";
    private static final String PARAM_NAME = "name";

    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        HttpSession session = request.getSession();
        UserService userService = UserService.getInstance();

        User user = (User)session.getAttribute(ATTR_USER);
        long id = user.getId();
        String name = request.getParameter(PARAM_NAME);
        String page;

        try {
            String errorMessage = userService.updateName(id, name);

            if (errorMessage == null) {
                user.setName(name);
                session.setAttribute(ATTR_USER, user);
                session.setAttribute(ATTR_MESSAGE, "message.saved");
                page = "command=page&page=settings";
            } else {
                request.setAttribute(ATTR_ERROR_MESSAGE, errorMessage);
                page = PageManager.getPage("settings");
            }
        } catch (MissingResourceException | ServiceException e) {
            throw new CommandException(e);
        }

        return page;
    }
}
