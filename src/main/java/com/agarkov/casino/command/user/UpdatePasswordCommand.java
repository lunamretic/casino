package com.agarkov.casino.command.user;

import com.agarkov.casino.command.ActionCommand;
import com.agarkov.casino.entity.User;
import com.agarkov.casino.exception.CommandException;
import com.agarkov.casino.exception.ServiceException;
import com.agarkov.casino.manager.PageManager;
import com.agarkov.casino.service.UserService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import java.util.MissingResourceException;

/**
 * The command updates the user's password.
 */
public class UpdatePasswordCommand implements ActionCommand {
    private static final String ATTR_ERROR_MESSAGE = "errorMessage";
    private static final String ATTR_MESSAGE = "message";
    private static final String ATTR_USER = "user";
    private static final String PARAM_CURRENT_PASSWORD = "current_password";
    private static final String PARAM_NEW_PASSWORD = "new_password";
    private static final String PARAM_NEW_PASSWORD_REPEAT = "new_password_repeat";

    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        HttpSession session = request.getSession();
        UserService userService = UserService.getInstance();

        User user = (User)session.getAttribute(ATTR_USER);
        long id = user.getId();
        String currentPassword = request.getParameter(PARAM_CURRENT_PASSWORD);
        String newPassword = request.getParameter(PARAM_NEW_PASSWORD);
        String newPasswordRepeat = request.getParameter(PARAM_NEW_PASSWORD_REPEAT);
        String username = user.getUsername();
        String page;

        try {
            String errorMessage = userService.updatePassword(id, currentPassword, newPassword, newPasswordRepeat, username);

            if (errorMessage == null) {
                page = "command=page&page=settings";
                session.setAttribute(ATTR_MESSAGE, "message.saved");
            } else {
                request.setAttribute(ATTR_ERROR_MESSAGE, errorMessage);
                page = PageManager.getPage("settings");
            }
        } catch (MissingResourceException | ServiceException e) {
            throw new CommandException(e);
        }

        return page;
    }
}