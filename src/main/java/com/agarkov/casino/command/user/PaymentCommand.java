package com.agarkov.casino.command.user;

import com.agarkov.casino.command.ActionCommand;
import com.agarkov.casino.entity.User;
import com.agarkov.casino.exception.CommandException;
import com.agarkov.casino.exception.ServiceException;
import com.agarkov.casino.manager.PageManager;
import com.agarkov.casino.service.UserService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * The command registers payment made by the user.
 */
public class PaymentCommand implements ActionCommand {
    private static final String ATTR_ERROR_MESSAGE = "errorMessage";
    private static final String ATTR_MESSAGE = "message";
    private static final String ATTR_USER = "user";
    private static final String PARAM_AMOUNT = "amount";

    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        HttpSession session = request.getSession();
        UserService userService = UserService.getInstance();

        User user = (User)session.getAttribute(ATTR_USER);
        long id = user.getId();
        String cash = request.getParameter(PARAM_AMOUNT);
        String page;

        try {
            String errorMessage = userService.depositCash(id, cash);
            int updatedCash = userService.findCash(id);

            if (errorMessage == null) {
                user.setCash(updatedCash);
                session.setAttribute(ATTR_USER, user);
                session.setAttribute(ATTR_MESSAGE, "message.saved");
                page = "command=page&page=payment";
            } else {
                request.setAttribute(ATTR_ERROR_MESSAGE, errorMessage);
                page = PageManager.getPage("payment");
            }
        } catch (ServiceException e) {
            throw new CommandException(e);
        }

        return page;
    }
}