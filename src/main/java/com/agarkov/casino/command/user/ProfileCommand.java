package com.agarkov.casino.command.user;

import com.agarkov.casino.command.ActionCommand;
import com.agarkov.casino.exception.CommandException;
import com.agarkov.casino.exception.ServiceException;
import com.agarkov.casino.manager.PageManager;
import com.agarkov.casino.service.InformationCardService;
import com.agarkov.casino.service.response.ProfileResponse;

import javax.servlet.http.HttpServletRequest;

/**
 * The command loads specific user profile.
 */
public class ProfileCommand implements ActionCommand {
    private static final String PARAM_USERNAME = "username";
    private static final String ATTR_INFO_CARD = "infoCard";
    private static final String ATTR_INVALID_FORM = "invalidForm";
    private static final String ATTR_USER_FOUND = "userFound";

    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        InformationCardService informationCardService = InformationCardService.getInstance();
        String username = request.getParameter(PARAM_USERNAME);

        ProfileResponse profileResponse;

        try {
            profileResponse = informationCardService.findUser(username);
        } catch (ServiceException e) {
            throw new CommandException(e);
        }

        if (profileResponse.isValid()) {
            if (profileResponse.getInformationCard() != null) {
                request.setAttribute(ATTR_INFO_CARD, profileResponse.getInformationCard());
                request.setAttribute(ATTR_USER_FOUND, true);
            } else {
                request.setAttribute(ATTR_USER_FOUND, false);
            }
        } else {
            request.setAttribute(ATTR_INVALID_FORM, true);
            request.setAttribute(ATTR_USER_FOUND, false);
        }


        return PageManager.getPage("profile");
    }
}
