package com.agarkov.casino.command.news;

import com.agarkov.casino.command.ActionCommand;
import com.agarkov.casino.entity.News;
import com.agarkov.casino.exception.CommandException;
import com.agarkov.casino.exception.ServiceException;
import com.agarkov.casino.manager.PageManager;
import com.agarkov.casino.service.NewsService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * The command sets the list of news for news (main) page of website.
 */
public class NewsCommand implements ActionCommand {
    private static final String ATTR_ADD_NEWS_ERROR = "invalid_form";
    private static final String ATTR_NEXT_PAGE = "next_page";
    private static final String ATTR_NEWS = "newsList";
    private static final String ATTR_PAGE = "page";
    private static final String PARAM_PAGE = "page";

    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        HttpSession session = request.getSession();
        NewsService newsService = NewsService.getInstance();

        int pageNews = Integer.parseInt(request.getParameter(PARAM_PAGE));

        List<News> newsList;

        try {
            newsList = newsService.loadPage(pageNews - 1);
        } catch (ServiceException e) {
            throw new CommandException(e);
        }

        if (newsList.size() > 5) {
            request.setAttribute(ATTR_NEXT_PAGE, true);
            newsList.remove(5);
        } else {
            request.setAttribute(ATTR_NEXT_PAGE, false);
        }
        request.setAttribute(ATTR_NEWS, newsList);
        request.setAttribute(ATTR_PAGE, pageNews);

        if (session.getAttribute(ATTR_ADD_NEWS_ERROR) != null && (boolean) session.getAttribute(ATTR_ADD_NEWS_ERROR)) {
            request.setAttribute(ATTR_ADD_NEWS_ERROR, true);
            session.removeAttribute(ATTR_ADD_NEWS_ERROR);
        }

        return PageManager.getPage("news");
    }
}