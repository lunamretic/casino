package com.agarkov.casino.command.news;

import com.agarkov.casino.command.ActionCommand;
import com.agarkov.casino.entity.User;
import com.agarkov.casino.exception.CommandException;
import com.agarkov.casino.exception.ServiceException;
import com.agarkov.casino.service.NewsService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * The command adds news to database.
 */
public class AddNewsCommand implements ActionCommand {
    private static final String ATTR_ADD_NEWS_ERROR = "invalid_form";
    private static final String ATTR_USER = "user";
    private static final String PARAM_BODY = "body";
    private static final String PARAM_HEADLINE = "headline";
    private static final String URI_PARAMS = "command=page&page=index";

    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        HttpSession session = request.getSession();
        NewsService newsService = NewsService.getInstance();

        User user = (User)session.getAttribute(ATTR_USER);
        long id = user.getId();
        String headline = request.getParameter(PARAM_HEADLINE);
        String body = request.getParameter(PARAM_BODY);

        boolean isValid;
        try {
            isValid = newsService.add(id, headline, body);
        } catch (ServiceException e) {
            throw new CommandException(e);
        }

        if (!isValid) {
            session.setAttribute(ATTR_ADD_NEWS_ERROR, true);
        }

        return URI_PARAMS;
    }
}
