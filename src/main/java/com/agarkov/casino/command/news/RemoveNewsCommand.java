package com.agarkov.casino.command.news;

import com.agarkov.casino.command.ActionCommand;
import com.agarkov.casino.exception.CommandException;
import com.agarkov.casino.exception.ServiceException;
import com.agarkov.casino.service.NewsService;

import javax.servlet.http.HttpServletRequest;

/**
 * The command delete specific news from database.
 */
public class RemoveNewsCommand implements ActionCommand {
    private static final String PARAM_ID = "id";
    private static final String URI_PARAMS = "command=page&page=index";

    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        NewsService newsService = NewsService.getInstance();

        long id = Long.parseLong(request.getParameter(PARAM_ID));

        try {
            newsService.remove(id);
        } catch (ServiceException e) {
            throw new CommandException(e);
        }

        return URI_PARAMS;
    }
}
