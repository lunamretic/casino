package com.agarkov.casino.command.game.preparation;

import com.agarkov.casino.command.GameCommand;
import com.agarkov.casino.entity.User;
import com.agarkov.casino.entity.game.response.GameSearchResponse;
import com.agarkov.casino.entity.game.response.ServerResponse;
import com.agarkov.casino.exception.CommandException;
import com.agarkov.casino.service.GameService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * The command searches opponent for a game.
 */
public class SearchOpponentCommand implements GameCommand {
    private static final String ATTR_USER = "user";
    private static final String ATTR_OPPONENT_NAME = "opponentName";
    private static final String ATTR_GAME_ID = "game_id";

    @Override
    public ServerResponse execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        HttpSession session = request.getSession();
        GameService gameService = GameService.getInstance();
        User user = (User) session.getAttribute(ATTR_USER);

        long userId = user.getId();
        String name = user.getName();
        int cash = user.getCash();

        GameSearchResponse gameSearchResponse = gameService.searchGame(userId, name, cash);

        if (gameSearchResponse != null) {
            session.setAttribute(ATTR_GAME_ID, gameSearchResponse.getGameId());
            session.setAttribute(ATTR_OPPONENT_NAME, gameSearchResponse.getOpponentName());
        }

        return gameSearchResponse;
    }
}
