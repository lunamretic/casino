package com.agarkov.casino.command.game.preparation;

import com.agarkov.casino.command.GameCommand;
import com.agarkov.casino.entity.User;
import com.agarkov.casino.entity.game.response.ServerResponse;
import com.agarkov.casino.exception.CommandException;
import com.agarkov.casino.service.GameService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * The command cancel search for a game.
 */
public class CancelSearchCommand implements GameCommand {
    private static final String ATTR_USER = "user";

    @Override
    public ServerResponse execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        HttpSession session = request.getSession();
        GameService gameService = GameService.getInstance();
        User user = (User) session.getAttribute(ATTR_USER);
        long id = user.getId();
        gameService.cancelSearch(id);
        return null;
    }
}
