package com.agarkov.casino.command.game;

import com.agarkov.casino.command.GameCommand;
import com.agarkov.casino.entity.User;
import com.agarkov.casino.entity.game.response.ServerResponse;
import com.agarkov.casino.exception.CommandException;
import com.agarkov.casino.exception.ServiceException;
import com.agarkov.casino.service.GameService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * The command accepts bet and notifies opponent of this.
 */
public class AcceptBetCommand implements GameCommand {
    private static final String ATTR_USER = "user";
    private static final String ATTR_GAME_ID = "game_id";

    @Override
    public ServerResponse execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        HttpSession session = request.getSession();
        GameService gameService = GameService.getInstance();
        User user = (User) session.getAttribute(ATTR_USER);

        long gameId = (long)session.getAttribute(ATTR_GAME_ID);
        long userId = user.getId();

        try {
            gameService.acceptBet(gameId, userId);
        } catch (ServiceException e) {
            throw new CommandException(e);
        }
        return null;
    }
}
