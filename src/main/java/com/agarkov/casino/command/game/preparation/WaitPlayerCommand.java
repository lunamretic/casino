package com.agarkov.casino.command.game.preparation;

import com.agarkov.casino.command.GameCommand;
import com.agarkov.casino.entity.User;
import com.agarkov.casino.entity.game.response.GameSearchResponse;
import com.agarkov.casino.entity.game.response.ServerResponse;
import com.agarkov.casino.exception.CommandException;
import com.agarkov.casino.exception.ServiceException;
import com.agarkov.casino.service.GameService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * The command determines whether the opponent was found for the game.
 */
public class WaitPlayerCommand implements GameCommand {
    private static final String ATTR_USER = "user";
    private static final String ATTR_GAME_ID = "game_id";
    private static final String ATTR_OPPONENT_NAME = "opponentName";

    @Override
    public ServerResponse execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        HttpSession session = request.getSession();
        GameService gameService = GameService.getInstance();
        GameSearchResponse gameSearchResponse;

        User user = (User) session.getAttribute(ATTR_USER);
        long userId = user.getId();
        try {
            gameSearchResponse = gameService.waitPlayer(userId);
        } catch (ServiceException e) {
            throw new CommandException(e);
        }

        if (gameSearchResponse != null) {
            session.setAttribute(ATTR_GAME_ID, gameSearchResponse.getGameId());
            session.setAttribute(ATTR_OPPONENT_NAME, gameSearchResponse.getOpponentName());
        }

        return gameSearchResponse;
    }
}
