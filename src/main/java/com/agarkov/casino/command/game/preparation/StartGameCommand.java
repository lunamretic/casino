package com.agarkov.casino.command.game.preparation;

import com.agarkov.casino.command.ActionCommand;
import com.agarkov.casino.exception.CommandException;
import com.agarkov.casino.manager.PageManager;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * The command starts the game vs player.
 */
public class StartGameCommand implements ActionCommand {
    private static final String ATTR_OPPONENT_NAME = "opponentName";
    private static final String ATTR_GAME_ID = "game_id";
    private static final String PARAM_GAME_ID = "id";

    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        HttpSession session = request.getSession();

        long gameId = Long.parseLong(request.getParameter(PARAM_GAME_ID));
        String opponentName = request.getParameter(ATTR_OPPONENT_NAME);
        request.setAttribute(ATTR_OPPONENT_NAME, opponentName);
        session.setAttribute(ATTR_GAME_ID, gameId);
        System.out.println("kek");

        return PageManager.getPage("game");
    }
}
