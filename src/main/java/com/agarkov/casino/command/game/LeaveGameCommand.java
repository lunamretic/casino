package com.agarkov.casino.command.game;

import com.agarkov.casino.command.GameCommand;
import com.agarkov.casino.entity.User;
import com.agarkov.casino.entity.game.response.ServerResponse;
import com.agarkov.casino.exception.CommandException;
import com.agarkov.casino.exception.ServiceException;
import com.agarkov.casino.service.GameService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * The command registers the user's exit from the game and notifies opponent of this.
 */
public class LeaveGameCommand implements GameCommand {
    private static final String ATTR_USER = "user";
    private static final String ATTR_GAME_ID = "game_id";

    @Override
    public ServerResponse execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        HttpSession session = request.getSession();
        GameService gameService = GameService.getInstance();
        User user = (User) session.getAttribute(ATTR_USER);

        long gameId = (long)session.getAttribute(ATTR_GAME_ID);
        long userId = user.getId();

        int cash;
        try {
            cash = gameService.leaveGame(gameId, userId);
        } catch (ServiceException e) {
            throw new CommandException(e);
        }

        if (cash != -1) {
            user.setCash(cash);
            session.setAttribute(ATTR_USER, user);
        }

        return null;
    }
}
