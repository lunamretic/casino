package com.agarkov.casino.command.game.preparation;

import com.agarkov.casino.command.ActionCommand;
import com.agarkov.casino.entity.User;
import com.agarkov.casino.exception.CommandException;
import com.agarkov.casino.manager.PageManager;
import com.agarkov.casino.service.GameService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * The command starts the game vs AI
 */
public class StartGameAICommand implements ActionCommand {
    private static final String ATTR_USER = "user";
    private static final String ATTR_GAME_ID = "game_id";
    private static final String ATTR_OPPONENT_NAME = "opponentName";
    private static final String OPPONENT_AI = "AI";

    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        HttpSession session = request.getSession();
        GameService gameService = GameService.getInstance();
        User user = (User) session.getAttribute(ATTR_USER);

        long userId = user.getId();
        String name = user.getName();
        int cash = user.getCash();
        long gameId = gameService.startGameAI(userId, name, cash);
        session.setAttribute(ATTR_GAME_ID, gameId);
        session.setAttribute(ATTR_OPPONENT_NAME, OPPONENT_AI);

        return PageManager.getPage("game");
    }
}
