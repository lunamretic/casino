package com.agarkov.casino.command;

import com.agarkov.casino.exception.CommandException;
import com.agarkov.casino.manager.PageManager;

import javax.servlet.http.HttpServletRequest;

/**
 * Specifies the page to which the user will be forwarded
 * if {@link com.agarkov.casino.command.ActionFactory ActionFactory}
 * class could not determine the command.
 */
public class EmptyCommand implements ActionCommand {
    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        return PageManager.getPage("index");
    }
}
