package com.agarkov.casino.command.authorization;

import com.agarkov.casino.command.ActionCommand;
import com.agarkov.casino.entity.User;
import com.agarkov.casino.exception.CommandException;
import com.agarkov.casino.exception.ServiceException;
import com.agarkov.casino.manager.PageManager;
import com.agarkov.casino.service.response.RegisterResponse;
import com.agarkov.casino.service.UserService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * The registration command.
 */
public class RegisterCommand implements ActionCommand {
    private static final String PARAM_NAME_USERNAME = "username";
    private static final String PARAM_NAME_EMAIL = "email";
    private static final String PARAM_NAME_PASSWORD = "password";
    private static final String PARAM_NAME_PASSWORD_REPEAT = "password-repeat";
    private static final String ATTR_ERROR_MESSAGE = "errorMessage";
    private static final String ATTR_USER = "user";


    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        HttpSession session = request.getSession();
        UserService userService = UserService.getInstance();



        String username = request.getParameter(PARAM_NAME_USERNAME);
        String email = request.getParameter(PARAM_NAME_EMAIL);
        String password = request.getParameter(PARAM_NAME_PASSWORD);
        String passwordRepeat = request.getParameter(PARAM_NAME_PASSWORD_REPEAT);

        RegisterResponse registerResponse;
        String page;

        try {
            registerResponse = userService.register(username, email, password, passwordRepeat);

            if (registerResponse.getErrorMessage() == null) {
                User user = registerResponse.getUser();
                session.setAttribute(ATTR_USER, user);
                page = "command=page&page=settings";
            } else {
                String errorMessage = registerResponse.getErrorMessage();
                request.setAttribute(ATTR_ERROR_MESSAGE, errorMessage);
                page = PageManager.getPage("registration");
            }
        } catch (ServiceException e) {
            throw new CommandException(e);
        }

        return page;
    }
}