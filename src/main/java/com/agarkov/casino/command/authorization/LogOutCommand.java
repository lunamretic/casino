package com.agarkov.casino.command.authorization;

import com.agarkov.casino.command.ActionCommand;
import com.agarkov.casino.exception.CommandException;
import com.agarkov.casino.manager.PageManager;

import javax.servlet.http.HttpServletRequest;

/**
 * The log out command.
 */
public class LogOutCommand implements ActionCommand {
    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        if (request.getSession(false) != null) {
            request.getSession().invalidate();
        }

        return PageManager.getPage("index");
    }
}
