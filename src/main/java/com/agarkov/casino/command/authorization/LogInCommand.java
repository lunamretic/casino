package com.agarkov.casino.command.authorization;

import com.agarkov.casino.command.ActionCommand;
import com.agarkov.casino.entity.User;
import com.agarkov.casino.exception.CommandException;
import com.agarkov.casino.exception.ServiceException;
import com.agarkov.casino.manager.PageManager;
import com.agarkov.casino.service.UserService;
import com.agarkov.casino.validator.FormValidator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * The authorization command.
 */
public class LogInCommand implements ActionCommand {
    private static final String PARAM_NAME_USERNAME = "username";
    private static final String PARAM_NAME_PASSWORD = "password";
    private static final String ATTR_ERROR_MESSAGE = "errorMessage";
    private static final String ATTR_USER = "user";

    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        HttpSession session = request.getSession();
        UserService userService = UserService.getInstance();

        String username = request.getParameter(PARAM_NAME_USERNAME);
        String password = request.getParameter(PARAM_NAME_PASSWORD);

        String errorMessage = FormValidator.validateAuthentication(username, password);

        User user;
        String page;
        try {
            if (errorMessage != null) {
                request.setAttribute(ATTR_ERROR_MESSAGE, errorMessage);
                page = PageManager.getPage("log-in");
            } else {
                user = userService.logIn(username, password);

                if (user != null) {
                    session.setAttribute(ATTR_USER, user);
                    page = "command=page&page=index";
                } else {
                    request.setAttribute(ATTR_ERROR_MESSAGE, "message.error-authorization");
                    page = PageManager.getPage("log-in");
                }
            }
        } catch (ServiceException e) {
            throw new CommandException(e);
        }
        return page;
    }
}