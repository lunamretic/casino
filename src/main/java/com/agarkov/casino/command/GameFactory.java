package com.agarkov.casino.command;

import javax.servlet.http.HttpServletRequest;

/**
 * The type Game factory.
 */
public class GameFactory {
    private static final String COMMAND_PARAM = "command";

    /** Do not instantiate GameFactory. */
    private GameFactory() {
        throw new IllegalAccessError("Utility class");
    }

    /**
     * Define command for execution.
     *
     * @param request the HttpServletRequest
     * @return the game command
     */
    public static GameCommand defineCommand(HttpServletRequest request) {
        GameCommand currentCommand;
        String action = request.getParameter(COMMAND_PARAM);
        if (action == null || action.isEmpty()) {
            return null;
        }
        GameCommandEnum currentEnum = GameCommandEnum.valueOf(action.toUpperCase());
        currentCommand = currentEnum.getCurrentCommand();
        return currentCommand;
    }
}
