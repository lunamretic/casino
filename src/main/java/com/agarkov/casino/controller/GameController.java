package com.agarkov.casino.controller;

import com.agarkov.casino.command.GameCommand;
import com.agarkov.casino.command.GameFactory;
import com.agarkov.casino.entity.game.response.ServerResponse;
import com.agarkov.casino.exception.CommandException;
import com.agarkov.casino.manager.PageManager;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * The type Game controller.
 * Manages AJAX requests
 */
@WebServlet("/game")
public class GameController extends HttpServlet {
    private static final Logger LOG = Logger.getLogger(GameController.class);
    private static final String ATTR_ERROR_MESSAGE = "errorMessage";

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    private void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        GameCommand ajaxCommand = GameFactory.defineCommand(request);
        ServerResponse serverResponse = null;
        try {
            serverResponse = ajaxCommand.execute(request, response);
        } catch (CommandException e) {
            LOG.error(e);
            String page = PageManager.getPage("error500");
            request.setAttribute(ATTR_ERROR_MESSAGE, e);
            request.getRequestDispatcher(page).forward(request, response);
        }
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(response.getOutputStream(), serverResponse);
    }
}
