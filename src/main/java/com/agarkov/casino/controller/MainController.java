package com.agarkov.casino.controller;

import com.agarkov.casino.command.ActionCommand;
import com.agarkov.casino.command.ActionFactory;
import com.agarkov.casino.exception.CommandException;
import com.agarkov.casino.pool.ConnectionPool;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.MissingResourceException;

/**
 * The type Main controller.
 * Manages all request from user except AJAX and Avatar requests
 */
@WebServlet(urlPatterns = {"/account", "/authorization", "/controller", "/play"})
public class MainController extends HttpServlet {
    private static final Logger LOG = Logger.getLogger(MainController.class);
    private static final String ATTR_ERROR_MESSAGE = "errorMessage";

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String page = processRequest(request, response);
        if (request.getAttribute(ATTR_ERROR_MESSAGE) == null) {
            String servlet = request.getServletPath();
            response.sendRedirect(servlet.concat("?").concat(page));
        } else {
            request.getRequestDispatcher(page).forward(request, response);
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String page = processRequest(request, response);
        request.getRequestDispatcher(page).forward(request, response);
    }

    private String processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        ActionCommand actionCommand = ActionFactory.defineCommand(request);

        String page;
        try {
            page = actionCommand.execute(request);
        } catch (CommandException e) {
            LOG.error(e);
            page = "/jsp/error/error_500.jsp";
            request.setAttribute(ATTR_ERROR_MESSAGE, e);
        } catch (MissingResourceException e) {
            LOG.error(e);
            page = "/jsp/error/error_404.jsp";
            request.setAttribute(ATTR_ERROR_MESSAGE, e);
        }

        return page;
    }

    @Override
    public void init() throws ServletException {
        super.init();
        ConnectionPool.getInstance();
        LOG.info("Server started.");
    }

    @Override
    public void destroy() {
        super.destroy();
        ConnectionPool.closePool();
        LOG.info("Server stopped.");
    }
}
