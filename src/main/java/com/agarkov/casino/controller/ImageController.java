package com.agarkov.casino.controller;

import com.agarkov.casino.entity.User;
import com.agarkov.casino.manager.PageManager;
import com.agarkov.casino.service.UserService;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;

/**
 * The type Image controller.
 */
@WebServlet("/image")
public class ImageController extends HttpServlet {
    private static final Logger LOG = Logger.getLogger(ImageController.class);
    private static final String ATTR_ERROR_MESSAGE = "errorMessage";
    private static final String ATTR_MESSAGE = "message";
    private static final String ATTR_USER = "user";
    private static final String PATH_SRC = "D:\\Projects\\epam\\course project\\casino\\src\\main\\webapp\\img\\avatars\\";
    private static final String PATH_TARGET = "\\img\\avatars";

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        UserService userService = UserService.getInstance();
        HttpSession session = request.getSession();
        String page;

        boolean isMultipart = ServletFileUpload.isMultipartContent(request);
        if (isMultipart) {
            User user = (User)session.getAttribute(ATTR_USER);
            long id = user.getId();
            String name = null;

            try {
                List<FileItem> multiparts = new ServletFileUpload(new DiskFileItemFactory()).parseRequest(request);
                for (FileItem item : multiparts) {
                    if (!item.isFormField()) {
                        name = new File(item.getName()).getName();
                        String srcPath = PATH_SRC + id + File.separator + name;
                        File srcFile = new File(srcPath);
                        if(!srcFile.getParentFile().exists()) {
                            srcFile.getParentFile().mkdir();
                        }
                        item.write(srcFile);
                        String targetPath = getServletContext().getRealPath(PATH_TARGET) + File.separator + id + File.separator + name;
                        File targetFile = new File(targetPath);
                        if (!targetFile.exists()) {
                            Files.copy(srcFile.toPath(), targetFile.toPath());
                        }
                    }
                }
                String filePath = id + File.separator + name;
                userService.updateAvatar(id, filePath);
                user.setImageSrc(filePath);

                session.setAttribute(ATTR_USER, user);
                session.setAttribute(ATTR_MESSAGE, "message.saved");
                page = "/account?command=page&page=settings";
                response.sendRedirect(page);
            } catch (Exception e) {
                LOG.error(e.getMessage());
                page = PageManager.getPage("error500");
                request.setAttribute(ATTR_ERROR_MESSAGE, e);
                request.getRequestDispatcher(page).forward(request, response);
            }
        } else {
            request.setAttribute(ATTR_ERROR_MESSAGE, "message.error_update_avatar");
            page = PageManager.getPage("settings");
            request.getRequestDispatcher(page).forward(request, response);
        }
    }
}
