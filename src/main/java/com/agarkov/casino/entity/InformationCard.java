package com.agarkov.casino.entity;

/**
 * The type Information card.
 * Contains public information about user
 */
public class InformationCard implements Entity {
    private int number;
    private String username;
    private String email;
    private String name;
    private Role role;
    private String imageSrc;
    private int win;
    private int lose;
    private int draw;
    private double winRate;

    /**
     * Gets number.
     *
     * @return the number
     */
    public int getNumber() {
        return number;
    }

    /**
     * Sets number.
     *
     * @param number the number
     */
    public void setNumber(int number) {
        this.number = number;
    }

    /**
     * Gets username.
     *
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * Sets username.
     *
     * @param username the username
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * Gets email.
     *
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * Sets email.
     *
     * @param email the email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * Gets name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets name.
     *
     * @param name the name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets role.
     *
     * @return the role
     */
    public Role getRole() {
        return role;
    }

    /**
     * Sets role.
     *
     * @param role the role
     */
    public void setRole(Role role) {
        this.role = role;
    }

    /**
     * Gets image src.
     *
     * @return the image src
     */
    public String getImageSrc() {
        return imageSrc;
    }

    /**
     * Sets image src.
     *
     * @param imageSrc the image src
     */
    public void setImageSrc(String imageSrc) {
        this.imageSrc = imageSrc;
    }

    /**
     * Gets win.
     *
     * @return the win
     */
    public int getWin() {
        return win;
    }

    /**
     * Sets win.
     *
     * @param win the win
     */
    public void setWin(int win) {
        this.win = win;
    }

    /**
     * Gets lose.
     *
     * @return the lose
     */
    public int getLose() {
        return lose;
    }

    /**
     * Sets lose.
     *
     * @param lose the lose
     */
    public void setLose(int lose) {
        this.lose = lose;
    }

    /**
     * Gets draw.
     *
     * @return the draw
     */
    public int getDraw() {
        return draw;
    }

    /**
     * Sets draw.
     *
     * @param draw the draw
     */
    public void setDraw(int draw) {
        this.draw = draw;
    }

    /**
     * Gets win rate.
     *
     * @return the win rate
     */
    public double getWinRate() {
        return winRate;
    }

    /**
     * Sets win rate.
     *
     * @param winRate the win rate
     */
    public void setWinRate(double winRate) {
        this.winRate = winRate;
    }
}
