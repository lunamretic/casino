package com.agarkov.casino.entity.game.response;

/**
 * The type Define turn response.
 */
public class DefineTurnResponse implements ServerResponse {
    private boolean isAI;
    private boolean isPlayerTurn;

    /**
     * Is ai boolean.
     *
     * @return the boolean
     */
    public boolean isAI() {
        return isAI;
    }

    /**
     * Sets ai.
     *
     * @param ai the ai
     */
    public void setAI(boolean ai) {
        isAI = ai;
    }

    /**
     * Is player turn boolean.
     *
     * @return the boolean
     */
    public boolean isPlayerTurn() {
        return isPlayerTurn;
    }

    /**
     * Sets player turn.
     *
     * @param playerTurn the player turn
     */
    public void setPlayerTurn(boolean playerTurn) {
        isPlayerTurn = playerTurn;
    }
}
