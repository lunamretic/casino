package com.agarkov.casino.entity.game;

/**
 * The enum Rank.
 */
public enum Rank {
    /**
     * Six rank.
     */
    SIX(6),
    /**
     * Seven rank.
     */
    SEVEN(7),
    /**
     * Eight rank.
     */
    EIGHT(8),
    /**
     * Nine rank.
     */
    NINE(9),
    /**
     * Ten rank.
     */
    TEN(10),
    /**
     * Jack rank.
     */
    JACK(2),
    /**
     * Queen rank.
     */
    QUEEN(3),
    /**
     * King rank.
     */
    KING(4),
    /**
     * Ace rank.
     */
    ACE(11);

    private int value;

    Rank(final int value) {
        this.value = value;
    }

    /**
     * Gets value.
     *
     * @return the value
     */
    public int getValue() {
        return value;
    }
}
