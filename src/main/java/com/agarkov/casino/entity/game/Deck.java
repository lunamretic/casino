package com.agarkov.casino.entity.game;

import com.agarkov.casino.entity.Entity;

import java.util.LinkedList;

/**
 * The type Deck.
 */
public class Deck implements Entity {
    private LinkedList<Card> deck;

    /**
     * Instantiates a new Deck.
     *
     * @param cards the cards
     */
    public Deck(LinkedList<Card> cards) {
        this.deck = cards;
    }

    /**
     * Gets deck.
     *
     * @return the deck
     */
    public LinkedList<Card> getDeck() {
        return deck;
    }

    /**
     * Draw card from deck.
     *
     * @return the top card
     */
    public Card drawCard() {
        return deck.removeFirst();
    }
}
