package com.agarkov.casino.entity.game.response;

/**
 * The type Polling response.
 */
public class PollingResponse implements ServerResponse {
    private String type;

    /**
     * Instantiates a new Polling response.
     */
    public PollingResponse() {}

    /**
     * Instantiates a new Polling response.
     *
     * @param type the type
     */
    public PollingResponse(String type) {
        this.type = type;
    }

    /**
     * Gets type.
     *
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * Sets type.
     *
     * @param type the type
     */
    public void setType(String type) {
        this.type = type;
    }
}
