package com.agarkov.casino.entity.game;

import com.agarkov.casino.entity.Entity;

/**
 * The type Card.
 */
public class Card implements Entity {
    private Suit suit;
    private Rank rank;
    private int value;
    private String imgSrc;

    /**
     * Gets suit.
     *
     * @return the suit
     */
    public Suit getSuit() {
        return suit;
    }

    /**
     * Sets suit.
     *
     * @param suit the suit
     */
    public void setSuit(Suit suit) {
        this.suit = suit;
    }

    /**
     * Gets rank.
     *
     * @return the rank
     */
    public Rank getRank() {
        return rank;
    }

    /**
     * Sets rank.
     *
     * @param rank the rank
     */
    public void setRank(Rank rank) {
        this.rank = rank;
    }

    /**
     * Gets value.
     *
     * @return the value
     */
    public int getValue() {
        return value;
    }

    /**
     * Sets value.
     *
     * @param value the value
     */
    public void setValue(int value) {
        this.value = value;
    }

    /**
     * Gets img src.
     *
     * @return the img src
     */
    public String getImgSrc() {
        return imgSrc;
    }

    /**
     * Sets img src.
     *
     * @param imgSrc the img src
     */
    public void setImgSrc(String imgSrc) {
        this.imgSrc = imgSrc;
    }
}
