package com.agarkov.casino.entity.game.response;

import com.agarkov.casino.entity.game.Card;

/**
 * The type Draw card response.
 */
public class DrawCardResponse implements ServerResponse {
    private Card card;
    private int points;

    /**
     * Sets card.
     *
     * @param card the card
     */
    public void setCard(Card card) {
        this.card = card;
    }

    /**
     * Sets points.
     *
     * @param points the points
     */
    public void setPoints(int points) {
        this.points = points;
    }

    /**
     * Gets card.
     *
     * @return the card
     */
    public Card getCard() {
        return card;
    }

    /**
     * Gets points.
     *
     * @return the points
     */
    public int getPoints() {
        return points;
    }
}
