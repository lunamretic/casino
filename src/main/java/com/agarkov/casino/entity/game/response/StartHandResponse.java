package com.agarkov.casino.entity.game.response;

import com.agarkov.casino.entity.game.Card;

import java.util.ArrayList;

/**
 * The type Start hand response.
 */
public class StartHandResponse implements ServerResponse {
    private ArrayList<Card> startHand;
    private int points;

    /**
     * Gets start hand.
     *
     * @return the start hand
     */
    public ArrayList<Card> getStartHand() {
        return startHand;
    }

    /**
     * Sets start hand.
     *
     * @param startHand the start hand
     */
    public void setStartHand(ArrayList<Card> startHand) {
        this.startHand = startHand;
    }

    /**
     * Gets points.
     *
     * @return the points
     */
    public int getPoints() {
        return points;
    }

    /**
     * Sets points.
     *
     * @param points the points
     */
    public void setPoints(int points) {
        this.points = points;
    }

}
