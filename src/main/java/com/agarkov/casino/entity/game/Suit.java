package com.agarkov.casino.entity.game;

/**
 * The enum Suit.
 */
public enum Suit {
    /**
     * Spades suit.
     */
    SPADES,
    /**
     * Hearts suit.
     */
    HEARTS,
    /**
     * Diamonds suit.
     */
    DIAMONDS,
    /**
     * Clubs suit.
     */
    CLUBS
}
