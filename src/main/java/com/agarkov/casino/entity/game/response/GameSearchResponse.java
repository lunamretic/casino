package com.agarkov.casino.entity.game.response;

/**
 * The type Game search response.
 */
public class GameSearchResponse implements ServerResponse {
    private long gameId;
    private String opponentName;

    /**
     * Gets game id.
     *
     * @return the game id
     */
    public long getGameId() {
        return gameId;
    }

    /**
     * Sets game id.
     *
     * @param gameId the game id
     */
    public void setGameId(long gameId) {
        this.gameId = gameId;
    }

    /**
     * Gets opponent name.
     *
     * @return the opponent name
     */
    public String getOpponentName() {
        return opponentName;
    }

    /**
     * Sets opponent name.
     *
     * @param opponentName the opponent name
     */
    public void setOpponentName(String opponentName) {
        this.opponentName = opponentName;
    }
}
