package com.agarkov.casino.entity.game.response;

import com.agarkov.casino.entity.game.Card;

import java.util.ArrayList;

/**
 * The type Stand response.
 */
public class StandResponse implements ServerResponse {
    private int cash;
    private String type;
    private int opponentPoints;
    private ArrayList<Card> opponentHand;
    private String message;


    /**
     * Gets type.
     *
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * Sets type.
     *
     * @param type the type
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * Gets opponent points.
     *
     * @return the opponent points
     */
    public int getOpponentPoints() {
        return opponentPoints;
    }

    /**
     * Sets opponent points.
     *
     * @param opponentPoints the opponent points
     */
    public void setOpponentPoints(int opponentPoints) {
        this.opponentPoints = opponentPoints;
    }

    /**
     * Gets opponent hand.
     *
     * @return the opponent hand
     */
    public ArrayList<Card> getOpponentHand() {
        return opponentHand;
    }

    /**
     * Sets opponent hand.
     *
     * @param opponentHand the opponent hand
     */
    public void setOpponentHand(ArrayList<Card> opponentHand) {
        this.opponentHand = opponentHand;
    }

    /**
     * Gets message.
     *
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * Sets message.
     *
     * @param message the message
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * Gets cash.
     *
     * @return the cash
     */
    public int getCash() {
        return cash;
    }

    /**
     * Sets cash.
     *
     * @param cash the cash
     */
    public void setCash(int cash) {
        this.cash = cash;
    }
}
