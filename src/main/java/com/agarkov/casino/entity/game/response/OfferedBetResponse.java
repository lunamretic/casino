package com.agarkov.casino.entity.game.response;

/**
 * The type Offered bet response.
 */
public class OfferedBetResponse implements ServerResponse {
    private String type;
    private int bet;

    /**
     * Instantiates a new Offered bet response.
     *
     * @param type the type
     * @param bet  the bet
     */
    public OfferedBetResponse(String type, int bet) {
        this.type = type;
        this.bet = bet;
    }

    /**
     * Gets type.
     *
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * Sets type.
     *
     * @param type the type
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * Gets bet.
     *
     * @return the bet
     */
    public int getBet() {
        return bet;
    }

    /**
     * Sets bet.
     *
     * @param bet the bet
     */
    public void setBet(int bet) {
        this.bet = bet;
    }
}
