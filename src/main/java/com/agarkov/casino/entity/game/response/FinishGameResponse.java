package com.agarkov.casino.entity.game.response;

/**
 * The type Finish game response.
 */
public class FinishGameResponse implements ServerResponse {
    private String type;

    /**
     * Instantiates a new Finish game response.
     *
     * @param type the type
     */
    public FinishGameResponse(String type) {
        this.type = type;
    }

    /**
     * Gets type.
     *
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * Sets type.
     *
     * @param type the type
     */
    public void setType(String type) {
        this.type = type;
    }
}
