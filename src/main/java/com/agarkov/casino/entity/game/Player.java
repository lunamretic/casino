package com.agarkov.casino.entity.game;

import com.agarkov.casino.entity.Entity;

import java.util.ArrayList;

/**
 * The type Player.
 */
public class Player implements Entity {
    private long id;
    private String username;
    private ArrayList<Card> hand;
    private int points;
    private int cash;
    private int number;

    /**
     * Instantiates a new Player.
     */
    public Player() {
        hand = new ArrayList<>(5);
    }

    /**
     * Gets id.
     *
     * @return the id
     */
    public long getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param id the id
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * Gets username.
     *
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * Sets username.
     *
     * @param username the username
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * Gets hand.
     *
     * @return the hand
     */
    public ArrayList<Card> getHand() {
        return hand;
    }

    /**
     * Sets hand.
     *
     * @param hand the hand
     */
    public void setHand(ArrayList<Card> hand) {
        this.hand = hand;
    }

    /**
     * Gets points.
     *
     * @return the points
     */
    public int getPoints() {
        return points;
    }

    /**
     * Sets points.
     *
     * @param points the points
     */
    public void setPoints(int points) {
        this.points = points;
    }

    /**
     * Gets cash.
     *
     * @return the cash
     */
    public int getCash() {
        return cash;
    }

    /**
     * Sets cash.
     *
     * @param cash the cash
     */
    public void setCash(int cash) {
        this.cash = cash;
    }

    /**
     * Gets number.
     *
     * @return the number
     */
    public int getNumber() {
        return number;
    }

    /**
     * Sets number.
     *
     * @param number the number
     */
    public void setNumber(int number) {
        this.number = number;
    }

    /**
     * Take card.
     *
     * @param card the card
     */
    public void takeCard(Card card) {
        hand.add(card);
        points += card.getValue();
    }

    /**
     * Clear hand.
     */
    public void clearHand() {
        points = 0;
        hand.clear();
    }
}
