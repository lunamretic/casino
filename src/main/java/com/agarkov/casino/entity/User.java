package com.agarkov.casino.entity;

import java.io.Serializable;

/**
 * The type User.
 */
public class User implements Entity, Serializable {
    private long id;
    private String username;
    private String email;
    private String name;
    private Role role;
    private int cash;
    private String imageSrc;

    /**
     * Instantiates a new User.
     */
    public User() {
        role = Role.USER;
        imageSrc = "\\noavatar.jpg";
    }

    /**
     * Gets id.
     *
     * @return the id
     */
    public long getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param id the id
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * Gets username.
     *
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * Sets username.
     *
     * @param username the username
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * Gets email.
     *
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * Sets email.
     *
     * @param email the email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * Gets name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets name.
     *
     * @param name the name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets role.
     *
     * @return the role
     */
    public Role getRole() {
        return role;
    }

    /**
     * Sets role.
     *
     * @param role the role
     */
    public void setRole(Role role) {
        this.role = role;
    }

    /**
     * Gets cash.
     *
     * @return the cash
     */
    public int getCash() {
        return cash;
    }

    /**
     * Sets cash.
     *
     * @param cash the cash
     */
    public void setCash(int cash) {
        this.cash = cash;
    }

    /**
     * Gets image src.
     *
     * @return the image src
     */
    public String getImageSrc() {
        return imageSrc;
    }

    /**
     * Sets image src.
     *
     * @param imageSrc the image src
     */
    public void setImageSrc(String imageSrc) {
        this.imageSrc = imageSrc;
    }
}
