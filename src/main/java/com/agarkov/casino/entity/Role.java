package com.agarkov.casino.entity;

/**
 * The enum user's Role.
 */
public enum Role {
    /**
     * User role.
     */
    USER,
    /**
     * Admin role.
     */
    ADMIN,
    /**
     * Banned role.
     */
    BANNED
}
