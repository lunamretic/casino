package com.agarkov.casino.dao;

import org.junit.Assert;
import org.junit.Test;

public class UserDAOTest {
    @Test
    public void checkExistence() throws Exception {
        Assert.assertFalse(new UserDAO().checkExistence("Неттакого", "нет@email.com"));
    }
}