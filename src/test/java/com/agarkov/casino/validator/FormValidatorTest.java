package com.agarkov.casino.validator;

import org.junit.Assert;
import org.junit.Test;

public class FormValidatorTest {
    @Test
    public void validateCash() throws Exception {
        String cash = "username";
        Assert.assertTrue("message.wrong-cash-amount".equals(FormValidator.validateCash(cash)));

    }

    @Test
    public void validateName() throws Exception {
        String name = "123Mike";
        Assert.assertTrue("message.wrong-name".equals(FormValidator.validateName(name)));
    }

    @Test
    public void validateAuthentication() throws Exception {
        String username = "Михаил";
        String password = "Password123";
        Assert.assertNull(FormValidator.validateAuthentication(username, password));
    }

    @Test
    public void validateRegistration() throws Exception {
        String username = "Username";
        String email = "email@email.com";
        String password = "Password123";
        String passwordRepeat = "Password12345";
        Assert.assertTrue("message.password-mismatch".equals(FormValidator.validateRegistration(username, email, password, passwordRepeat)));
    }
}