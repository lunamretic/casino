package com.agarkov.casino.logic;

import com.agarkov.casino.entity.game.Player;
import com.agarkov.casino.exception.GameSessionException;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class GameSessionTest {
    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void drawCard() throws Exception {
        GameSession gameSession = new GameSession(1, new Player(), new Player(), false);
        thrown.expect(GameSessionException.class);
        thrown.expectMessage("Invalid user (-1) in game session!");
        gameSession.drawCard(-1);
    }

}