package com.agarkov.casino.manager;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.MissingResourceException;

public class PageManagerTest {
    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void getPage() throws Exception {
        thrown.expect(MissingResourceException.class);
        PageManager.getPage("not-a-page");
    }

    @Test
    public void getPage2() throws Exception {
        String page = PageManager.getPage("game");
        String actualPage = "/jsp/game/game.jsp";
        Assert.assertEquals(page, actualPage);
    }

}