package com.agarkov.casino.pool;

import org.junit.Assert;
import org.junit.Test;

public class ConnectionPoolTest {
    @Test
    public void getConnection() throws Exception {
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        Assert.assertNotNull(connectionPool.getConnection());
    }

    @Test
    public void getInstance() throws Exception {
        Assert.assertNotNull(ConnectionPool.getInstance());
    }

}